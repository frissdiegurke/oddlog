---
home: true
heroImage: /logo.png
actionText: Get Started →
actionLink: /guide/
features:
- title: Object driven
  details: Attach payload objects to your logger instances, attach payload objects to your logging message and all payloads will be merged together for the log record.
- title: Ultra fast
  details: The heaviest operations are evaluated lazily. Logging methods below the minimum action level are replaced with no-op functions.
- title: Equipped & extensible
  details: Transports for most common message processing are built-in; custom ones can be written easily. Suits every application and library.
footer: MIT Licensed | Copyright © 2016-2018 Ole Reglitzki
---

---

[![Preview](/oddlog/preview.png)](/oddlog/preview.png)

---

::: tip
[![Version](https://img.shields.io/npm/v/oddlog.svg)](https://www.npmjs.com/package/oddlog)
[![License](https://img.shields.io/npm/l/oddlog.svg?longCache=true)](https://gitlab.com/frissdiegurke/oddlog/blob/master/LICENSE)
[![build status](https://gitlab.com/frissdiegurke/oddlog/badges/master/build.svg)](https://gitlab.com/frissdiegurke/oddlog/commits/master)
[![coverage report](https://gitlab.com/frissdiegurke/oddlog/badges/master/coverage.svg)](https://gitlab.com/frissdiegurke/oddlog/commits/master)
:::

<style lang=stylus>
h2 {
  margin-top: 1rem;
}

.content > hr:first-child {
  margin-top: 0;
}

.content p:nth-child(2) {
  margin-top: 2.2rem;
  margin-bottom: 2.2rem;
  img {
    display: block;
    margin: auto;
    max-width: none;
  }
}

.custom-block.tip {
  padding: 0;
  border: none;
  background-color: transparent;

  .custom-block-title {
    display: none;
  }

  > p {
    display: flex;
    justify-content: space-around;
    line-height: 1;
  }

  a {
    height: 20px;

    > img {
      display: block;
    }
    > svg {
      display: none;
    }
  }
}
</style>
