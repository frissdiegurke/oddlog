# Performance considerations

While oddlog is a very fast logger, there are still some tricks to improve performance. This is especially useful for
logs that will be active in production as all logging methods are no-operation functions if no transport would actually
process the message.

## Use your environment

The level specification within the transport definition should in most cases be production targeted only. For
development, you can set the environment variables `DEBUG` and/or `TRACE` to [match]() your logger names.

::: tip
Use `DEBUG` and `TRACE` [environment variables](./environment-variables.md) for debugging; reserve levels within
transport definitions for production. 
:::

### Source location

Logging the location of the logging call within the source code has a significant impact on the performance. Do not use
this in production.

With [Issue #42](https://gitlab.com/frissdiegurke/oddlog/issues/42) resolved, source code location will be logged when
the environment variables match.

## Rotation and gzip

Since many parts of the payload within log records are redundant (see [inheritance](./inheritance.md)),
gzip is a natural fit for production logging. Oddlog has a file-rotation transport with gzip compression built-in. You
can specify a maximum file-size before rotation (and compression), and/or a maximum interval after which a rotation
needs to happen.

::: tip
For production, gzip is crucial; use the built-in file-rotation transport.
:::

## Keep it lazy

As they remove the lazy evaluation, the `immediate*` options should be avoided. If you do not modify payload after
logging, you do not need them anyways.

::: tip
Avoid the `immediate*` options for [deferred transports](./transports.md#deferredtransport) whenever possible.
:::

## Deep children

As children creation is a heavy task, each logger should only have a distinct **depth** for its children. Also as long
as any child is referenced, some data (the absolute necessities) of the parent logger is not up for garbage collection.

::: tip
Do not create logger children of indistinct depth.
:::

### Cache replication

The [trigger transport](./transports.md#triggertransport) allows cache replication (children get dedicated cache). While
this is useful up to some depth, it might get dangerous for many children as the total available cache size is
multiplied by the amount of dedicated caches. Just keep this in mind when setting the `replicate` option.

## Asynchronous logging

As writing to `process.stdout` or `process.stderr`
[*can* be blocking](https://nodejs.org/api/process.html#process_a_note_on_process_i_o), heavy production applications
should avoid using those for significant amount of written log records. Use file transports in favor of piping
application output to a file.
