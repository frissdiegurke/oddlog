# Design decisions

## Application

> Which logs should appear on a users terminal?

The terminal output should basically have two purposes:

 1. Display information that is necessary for users.
 2. Display as detailed and helpful information on errors as possible.
    * Many users won't bother to search for log files in order to report an error/bug; displaying it lowers the bar.
    * For debugging during development this would be great as well.

At a first glance those seem to be contrary to each other, but oddlog makes it easy to fulfill both purposes at the same
time (see below).

> Should all logs be stored the same?

In most cases the answer should be **yes**. There are only a few exceptional cases involving the need for different log
targets (e.g. some logs need to be transferred via network).

::: tip
Do not over-complicate your transports. In many cases you just want one logger with a single transport and create
children for your other root-level loggers.
:::

> What about staging/development?

In regular applications, we recommend to use the same transports for all stages:
  * `FileRotateTransport`
  * `TriggerTransport{threshold:ERROR} -> [FileTransport, StreamTransport]` - just take care to properly set
    `replication`.
  * `StreamTransport{level:INFO/VERBOSE}`

This way you still have all logs (`DEBUG` upwards) available, but for the most issues the bundled logs help you to debug
way faster.

## Library

Libraries should take care that none of the raw log records are subject to the application that uses the library. So no
raw log records should be streamed to `stdout`.

Use the `shy` flag for `createLogger` (or the transports within) to set appropriate defaults:

* Fallback transport level is raised to `WARN`.
* Fallback log format is set to `"simple"`, which provides a human readable short message (without payload).

If you need additional data for the logs that should get exposed to the end-user, use message formatting to fit it into
the message.

::: tip
Libraries should use the `shy` flag and avoid using levels below `WARN` in production (if any at all).
:::

> Should I use oddlog for libraries at all then?

Yes, logging can save you quite an amount of debugging time. For development and debugging, logs are worth a lot. Just
remember to turn off logging for production: `shy` flag if `WARN` and above should still be logged, replace logger with
no-op logger otherwise.

Especially when you remember to use the [environment variables](./environment-variables.md), logging in libraries is
just as great as within application. You even have the power to debug your libraries *on the field* when using the
environment variables.
