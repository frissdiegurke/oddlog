# Benchmarks

The detailed list of performance benchmarks, compared with bunyan and pino, can be seen [here](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/results.txt).

The benchmarks are designed with our best attempt to create the same conditions and for each library. All benchmarks
(except for the TriggerTransport specific one) use stream transports for log record writes. Shown below are those with
a single transport stream used.

As it can be seen below, pino and oddlog outperform bunyan for every operation. Oddlog outperforms pino for creation of
loggers and children. Pino outperforms oddlog for payload merge (especially for few to-be-merged attributes). Oddlog and
pino are head-to-head for logging without payload.

![create-and-log](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/create-and-log.png)

![child-and-log](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/child-and-log.png)

![silent](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/silent.png)

![create-and-message-silent](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/create-and-message-silent.png)

![logging-merge](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/logging-merge.png)

![logging-no-merge](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/logging-no-merge.png)

Oddlog had the advantage of in-process filters that highly outperform out-of-process filters due to oppressed writes to
the stream (compare to logging without payload merge above):

![trigger-transport](https://gitlab.com/frissdiegurke/oddlog/raw/master/benchmarks/charts/trigger-transport.png)
