# Transports

## Definition

As a method to deliver logs to their targets - e.g. the console, files, or nested transports -, oddlog allows you to
specify as many transport targets per logger as you desire.

The [`createLogger`](./README.md#createlogger)/[`defineLogger`](./README.md#definelogger) methods accept a
`{(?Object)[]} transports` attribute for the logger options. Each entry specifies some target for the logs. Depending on
the attributes, a transport is selected. In case the attributes are not specific enough (they usually are), you may
specify a `{String} type` to manually select a transport:

```text
type:"stream"       StreamTransport       aliases: { stream: process.stdout }
type:"file"         FileTransport        requires: { path: Path }
type:"cap"          CapTransport          aliases: { cap: oddlog.FATAL }
type:"memory"       MemoryTransport       aliases: { store: [] }
type:"trigger"      TriggerTransport      aliases: { threshold: oddlog.ERROR }
type:"file-rotate"  FileRotateTransport  requires: { path: Path, size: null, interval: null }
```

## Types

Listed here is a overview over the types, for a full list of options, take a look at the
[documentation](../documentation/transports.md).

Each Type accepts the following option:

| Option | Type | Description |
| --- | --- | --- |
| `level` | `Number|String` | default: `oddlog.DEBUG` - The minimum level of logs to process. |

### StreamTransport

Writes the log records to a stream.

| Option | Type | Description |
| --- | --- | --- |
| `stream` | `stream.Writable` | default: `process.stdout` - The stream to write to. |
| `closeStream` | `Boolean` | default: `false` - Whether to close the stream when the logger scope gets closed. |

### FileTransport

Writes the log records to a file.

| Option | Type | Description |
| --- | --- | --- |
| `path` | `String` | The file path to write to. |

### FileRotateTransport

Similar to [FileTransport](#filetransport), but does *rotate* files whenever a size or time limit is reached. Rotated
files get the suffix `.N` where `N` is the amount of rotations this file has gone through (e.g. `.2` contains earlier
logs than `.1`).

| Option | Type | Description |
| --- | --- | --- |
| `path` | `String` | The path of the main file to log to (no rotation suffix). |
| `size` | `Number|String` | If set, it determines the maximum file size (before rotation/compression). Accepts all common size descriptors or bytes as number. |
| `interval` | `Number|String` | If set, it determines the maximum time-span between rotations. Accepts all common time descriptors or milliseconds as number. |
| `maxFiles` | `Number` | If set, only up to the specified amount of files are stored; older logs will be removed. |
| `compress` | `Boolean` | default: `false` - Whether to use gzip for compression of rotated files. |

### CapTransport

Filters logs by maximum level.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. |
| `cap` | `Number|String` | default: `oddlog.FATAL` - The maximum level of logs allowed to be passed to nested transports. |

### SplitTransport

Decides to which nested transport to pass a log to, according to specified `split` levels.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. Every log is only passed to a single nested transport. |
| `split` | `(?Number|String)[]` | The list of threshold levels to decide which transport to use. E.g. `level < split[0]` will use the first transport, `split[0] <= level < split[1]` the second, ... |

### TriggerTransport

Caches logs until a threshold level is matched or exceeded by any incoming log. Whenever this triggers, the cached logs
are written to nested transports.

This transport is useful to keep logs bundled and thus provides an easy way to debug - by checking the log stack
directly leading to the triggering log.

| Option | Type | Description |
| --- | --- | --- |
| `transports` | `(?Object)[]` | A list of nested transport definitions. |
| `threshold` | `Number|String` | default: `oddlog.ERROR` - The minimum level to trigger writing the cached logs to nested transports. |
| `limit` | `Number` | default: `50` - The cache size (in logs). |
| `replicate` | `Boolean|Number` | default: `false` - If `true`, create dedicated caches for children. If a number is provided, create dedicated caches for children up of that depth. |

### MemoryTransport

Appends the log instances to the provided store. Useful for testing.

| Option | Type | Description |
| --- | --- | --- |
| `store` | `{push:Function(Message)}` | default: `[]` - The store to push logs to. |
