# Getting started

## Installation

The library oddlog should be added to the package.json file of your projects:

```bash
npm install --save oddlog
```

In order to beautify the output, you might want to install `oddlog-cli` either globally or project-specific (e.g. for
use within npm scripts).

```bash
npm install --save oddlog-cli
# or
npm install -g oddlog-cli # you probably need to run this as root
```

Import oddlog according to your module system:

```javascript
// ES6 modules
import * as oddlog from "oddlog";
// CommonJS
const oddlog = require("oddlog");
// AMD
require(["oddlog"], (oddlog) => { /* ... */ });
```

## Logger creation

Call `oddlog.createLogger` with your desired [options](../documentation/README.md#createlogger) like this:

```javascript
const myLogger = oddlog.createLogger("my-app", {transports: [{type: "stream"}]}, {some: "global payload"});
```

When using multiple loggers for an application, it is recommended to give all loggers the same prefix such as
`my-app:root`, `my-app:http`.

### Logger storage

If you'd like to use the built-in logger storage, you can replace `oddlog.createLogger` with `oddlog.defineLogger` and
use `oddlog.getLogger("my-app")` (within ES6 modules the default export is an alias for `getLogger`) to retrieve the
previously defined logger:

```javascript
oddlog.defineLogger("my-app", {transports: [{type: "stream"}]}, {some: "global payload"});

const myLogger = oddlog("my-app");
```

If you prefer to separate the logger name from the storage name, you can do so like this:

```javascript
// 1. create logger
const myHTTPLogger = oddlog.createLogger("my-app:http", {transports: [{type: "stream"}]}, {some: "global payload"});
// 2. register logger to the store
oddlog.defineLogger("http", myHTTPLogger);
// 3. retrieve logger from anywhere in your application
const httpLogger = oddlog("http");
```

## CLI

When you use `oddlog` within your project, the raw output looks like this (one line per log record):

```text
[1,"_type",["4.14.15-1-ARCH","zerg","26032"],"app",1,"2018-02-15T18:04:33.100Z",3,null,"server listening",{"host":"127.0.0.1","port":8128,"package":"oddlog-example-file-server@0.1.0","production":false}]
```

Pipe it through the `oddlog` binary that is provided by [`oddlog-cli`](https://gitlab.com/frissdiegurke/oddlog-cli) to
get human readable output instead:

![preview](/oddlog/preview.png)

Run `oddlog --help` for parameter information.

## Process exit

Especially in production it is highly important for all logs to be flushed on application exit - especially if caused by
errors that are not catched otherwise.

Oddlog provides some methods for this:

| Method | Description |
| --- | --- |
| [`Logger#close`](../documentation/logger.md#close) | Shuts down the logger and all its children. Calls a callback when done. Use this before calling `process.exit()` within your application. | 
| [`oddlog.closeAll`](../documentation/README.md#closeall) | Same as above for a bunch of passed loggers. Handy shortcut for above if multiple loggers are used. Call with all root-level loggers. |
| [`Logger#handleUncaughtExceptions`](../documentation/logger.md#handleuncaughtexceptions) | Attaches an `uncaughtException` event handler that logs the exception and closes the logger afterwards. Don't forget to close all other loggers within exit callback. |
| [`Logger#handleUnhandledRejections`](../documentation/logger.md#handleunhandledrejections) | Same as above for `unhandledRejection` events. |
| [`Logger#handleProcessEvents`](../documentation/logger.md#handleprocessevents) | Same as above for arbitrary process events, such as `SIGINT`. |
