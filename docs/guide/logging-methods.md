# Logging methods

## Levels

Each log record holds a level to represent its importance to the workflow. Although you could use any custom number
value, oddlog provides some predefined standard levels:

```
SILENT, TRACE, DEBUG, VERBOSE, INFO, WARN, ERROR, FATAL
```

It is best practice to scatter logging messages over as many levels as there are distinct *levels of significance*
within a module. If the module has some messages to tell that are more relevant than `INFO` but less relevant than
`WARN` it's completely fine to use a level like `4.5`.

## Methods

### Stateless

For each level of the above, exists one logging method (`Logger#silent`, `Logger#trace`, ...) that creates a new log
record.

The methods accept the following arguments (in this order):

| Parameter | Type | Description |
| --- | --- | --- |
| `ownPayload` | `Boolean` | default: `logger.ownChildPayloads` - Whether to permit oddlog to modify the provided payload. |
| `payload` | `?Object` | optional. The log payload. If set to `null`, no payload is inherited, otherwise it's merged with inherited payload. |
| `message` | `String` | optional. The primary message. Required, if `formatValues` are provided. |
| `formatValues` | `String...|Object` | optional. The format value(s) to apply to the primary message. See [`util.format()`](https://nodejs.org/api/util.html#util_util_format_format_args) |

Some examples:

```javascript
// log with payload (recommended for most cases)
logger.info(true, {sit: "amet"}, "Lorem ipsum dolor");
// log with formatted messages
logger.verbose("Hello, %s!", "World");
logger.warn("Inline payload: %O", {answer: 42});
```

### Stateful

As an alternative to the stateless logging methods, loggers also have stateful methods:

```javascript
// stateful methods
logger.setInfo();    // alias for logger.setLevel("info");
logger.log(/*...*/); // same arguments as for stateless methods
logger.done();       // reset the level to fallback (level option for createLogger/defineLogger)
// they can be chained too
logger
    .setDebug()
    .log(true, {some: "value"}, "here you are")
    .setTrace()
    .log("pew!")
    .done();
// the current level can also be obtained
const currentLevel = logger.getLevel(); // returns numerical level value
```

## Payload

Using payloads in **addition** to a ordinary log message has two major advantages:

1. It contains detailed and structured additional information.
2. It allows re-use of payload between multiple records.

All logging methods accept an optional `{?Object} payload` object before the `{String} message` parameter. The payload
will be merged with any inherited payload and attached to the log record. If `null` is provided, no payload will be
attached to the log record at all.

```javascript
logger.info(true, {port: 3000, host: "localhost"}, "server started");
```

::: tip
You may prepend a `true` parameter wherever you use pass inline payload. This allows oddlog to write into the payload
object instead of shallow-copy it first.
:::

::: tip
When you create heavy payload that might not end up being logged anyways, you may check the logger level first with the
[guarding methods](../documentation/logger.md#guarding-methods).
:::
