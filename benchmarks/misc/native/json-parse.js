/* eslint max-len: "off" */
"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("json");

let date = new Date();

let dateString = date.toISOString(), msgString = "application startup", level = 4;
let payload = {package: "oddlog-benchmarks@0.1.0", production: false}, payload2 = void 0;
let source0 = {file: "/some/path/log", col: 4, row: 15}, source1 = null, source2 = null;
let arr0 = [
  0,
  ["4.7.2-1-ARCH", "zerg", 5886],
  "app",
  dateString,
  level,
  source0,
  msgString,
  payload
];
let obj0 = {
  v: 0,
  t: ["4.7.2-1-ARCH", "zerg", 5886],
  n: "app",
  d: dateString,
  l: level,
  s: source0,
  m: msgString,
  p: payload
};

let arrString = JSON.stringify(arr0);
let objString = JSON.stringify(obj0);

suiteOddLog
  .add("Object ", () => JSON.parse(objString))
  .add("Array  ", () => JSON.parse(arrString))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  730K Array
  560K Object
*/
