"use strict";

const Benchmark = require("benchmark");
const __SLICE = Array.prototype.slice;

const suiteOddLog = new Benchmark.Suite("arguments-rest-parameter");

let obj = {hello: "world"};

suiteOddLog
  .add("Arguments#noSlice", () => withArguments(true, obj))
  .add("Arguments#slice  ", () => withArguments(true, obj, "Hello", "world", "something"))
  .add("Rest#noSlice     ", () => withoutArguments(true, obj))
  .add("Rest#slice       ", () => withoutArguments(true, obj, "Hello", "world", "something"))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function withArguments() {
  let result = {};
  let current = arguments[0], i = 1;
  if (typeof current === "boolean") {
    result.ownPayload = current;
    current = arguments[i++];
  }
  if (typeof current === "object") {
    result.payload = current;
    current = arguments[i++];
  }
  let _len = arguments.length;
  if (i <= _len) {
    if (typeof current !== "string") {
      throw new Error("Expected message to be a String; Got " + typeof current + ".");
    }
    result.message = i === _len ? current : getArguments(current, ...__SLICE.call(arguments, i));
  }
  return result;
}

function withoutArguments(...args) {
  let result = {};
  let current = args[0], i = 1;
  if (typeof current === "boolean") {
    result.ownPayload = current;
    current = args[i++];
  }
  if (typeof current === "object") {
    result.payload = current;
    current = args[i++];
  }
  let _len = args.length;
  if (i <= _len) {
    if (typeof current !== "string") {
      throw new Error("Expected message to be a String; Got " + typeof current + ".");
    }
    result.message = i === _len ? current : getArguments(current, ...args.slice(i));
  }
  return result;
}

function getArguments() { return arguments; }

// Rest#* won in both categories ~ factors 1.5 and 3.3
