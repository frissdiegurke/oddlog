"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("ceil");

const x = Math.random() * 0.4, y = Math.random() % 2, z = 0.5 + x;

suiteOddLog
  .add("Ceil#Math0", () => Math.ceil(x))
  .add("Ceil#Math1", () => Math.ceil(y))
  .add("Ceil#Math2", () => Math.ceil(z))
  .add("Ceil#Bina0", () => {
    let f = (x << 0);
    return f === x ? f : f + 1;
  })
  .add("Ceil#Bina1", () => {
    let f = (y << 0);
    return f === y ? f : f + 1;
  })
  .add("Ceil#Bina2", () => {
    let f = (z << 0);
    return f === z ? f : f + 1;
  })
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

// Binary wins, especially for float inputs
// Update node@10.6.0: Draw
