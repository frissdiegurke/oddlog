"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("reflect");

let array = ["hello", 3];
let obj = {};

suiteOddLog
// fixed array size
  .add("a Fn#simple           ", () => test(array[0], array[1]))
  .add("a Call#simple         ", () => test.call(obj, array[0], array[1]))
  .add("a Reflect.apply       ", () => Reflect.apply(test, obj, [array[0], array[1]]))
  // fixed array
  .add("b Fn#spread           ", () => test(...array))
  .add("b Call#spread         ", () => test.call(...array))
  .add("b Apply               ", () => test.apply(null, array))
  .add("b Reflect.apply       ", () => Reflect.apply(test, null, array))
  // unshift array
  .add("c Fn#spread           ", () => test("test", ...array))
  .add("c Call#spread         ", () => test.call(obj, "test", ...array))
  .add("c Apply#concat        ", () => test.apply(null, ["test"].concat(array)))
  .add("c Reflect.apply#concat", () => Reflect.apply(test, null, ["test"].concat(array)))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function test() {
  return arguments;
}

/*
node@10.6.0

  Group a:
    685M ops/s: Fn, Call
    38M ops/s: Reflect
  Group b:
    51M ops/s: Fn, Apply, Reflect
    39M ops/s: Call
  Group c:
    47M ops/s: Fn
    33M ops/s: Call
    6M ops/s: Apply, Reflect
*/
