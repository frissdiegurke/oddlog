"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("transformer-schema");

let array = [2, 6];
let object = {key: 2, value: 6};
let map = new Map();
map.set("key", 2);
map.set("value", 6);

const KEY = "key", VALUE = "value";

suiteOddLog
  .add("Map0   ", () => map.get("key") + map.get("value"))
  .add("Map1   ", () => map.get(KEY) + map.get(VALUE))
  .add("Object0", () => object.key + object.value)
  .add("Object1", () => object[KEY] + object[VALUE])
  .add("Array  ", () => array[0] + array[1])
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

// Object0 won
// Update node@10.6.0: Object0 slightly better than {Object1,Array} 8x better than Map
