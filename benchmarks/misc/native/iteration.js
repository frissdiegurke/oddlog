"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("iteration");

let set = new Set();
let array = [];
let object = {};

for (let i = 0; i < 1000; i++) {
  set.add(i);
  array.push(i);
  object[i] = null;
}

suiteOddLog
  .add("Array#of", () => { for (let val of array) { val.toString(); } })
  .add("Array#fe", () => { array.forEach((val) => val.toString()); })
  .add("Array#i0", () => { for (let i = 0; i < array.length; i++) { array[i].toString(); } })
  .add("Array#i1", () => {
    let _len = array.length;
    for (let i = 0; i < _len; i++) { array[i].toString(); }
  })
  .add("Array#i2", () => {
    let _len = array.length;
    for (let i = 0; i < _len; i++) {
      let item = array[i];
      item.toString();
    }
  })
  .add("Array#i3", () => {
    let _len = array.length, item, i;
    for (i = 0; i < _len; i++) {
      item = array[i];
      item.toString();
    }
  })
  .add("Array#--", () => {
    for (let i = array.length - 1; i >= 0; i--) {
      let item = array[i];
      item.toString();
    }
  })
  .add("Set     ", () => { for (let val of set) { val.toString(); } })
  .add("Object  ", () => { for (let key in object) { key.toString(); } })
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

// Set won
// Object better than Array
// Array#of worst, others insignificant difference
// Update node@10.6.0: Object worse by factor of 2; Array and Set draw; Array method insignificant

