"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("reflect");

suiteOddLog
// fixed array size
  .add("AsyncAwait", async () => await asyncFunction())
  .add("Async.then", () => asyncFunction().then(() => {}))
  .add("Callback  ", () => cbFunction(() => {}))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

async function asyncFunction() { return arguments; }

function cbFunction (cb) { cb(); }

/*
node@10.6.0
  680M ops/s: Callback
    3M ops/s: Async.then
    1M ops/s: AsyncAwait
*/
