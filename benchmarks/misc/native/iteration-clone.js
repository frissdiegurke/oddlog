"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("iteration-clone");

let set = new Set();
let array = [];
let object = {};

for (let i = 0; i < 5; i++) {
  set.add(i);
  array.push(i);
  object[i] = null;
}

suiteOddLog
  .add("Array ", () => {
    let clone = [];
    let _len = array.length;
    for (let i = 0; i < _len; i++) { clone.push(array[i]); }
  })
  .add("Set   ", () => {
    let clone = new Set();
    for (let val of set) { clone.add(val); }
  })
  .add("Object", () => {
    let clone = {};
    for (let key in object) { clone[key] = null; }
  })
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

// Array won (by factor >10)
// Set better than Object
