/* eslint max-len: "off" */
"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("json");

let date = new Date();

let dateString = date.toISOString(), msgString = "application startup", level = 4;
let payload = {package: "oddlog-benchmarks@0.1.0", production: false}, payload2 = void 0;
let source0 = {file: "/some/path/log", col: 4, row: 15}, source1 = null, source2 = null;
let arr0 = [
  0,
  ["4.7.2-1-ARCH", "zerg", 5886],
  "app",
  dateString,
  level,
  source0,
  msgString,
  payload
], arr1 = [
  0,
  ["4.7.2-1-ARCH", "zerg", 5886],
  "app",
  dateString,
  level,
  null,
  msgString,
  payload
], arr2 = [
  0,
  ["4.7.2-1-ARCH", "zerg", 5886],
  "app",
  dateString,
  level,
  null,
  msgString
];
let obj0 = {
  v: 0,
  t: ["4.7.2-1-ARCH", "zerg", 5886],
  n: "app",
  d: dateString,
  l: level,
  s: source0,
  m: msgString,
  p: payload
}, obj1 = {
  v: 0,
  t: ["4.7.2-1-ARCH", "zerg", 5886],
  n: "app",
  d: dateString,
  l: level,
  m: msgString,
  p: payload
}, obj2 = {
  v: 0,
  t: ["4.7.2-1-ARCH", "zerg", 5886],
  n: "app",
  d: dateString,
  l: level,
  m: msgString
};

let prefix = "[0,[\"4.7.2-1-ARCH\",\"zerg\",5886],\"app\",";

suiteOddLog
  .add("0#Object", () => JSON.stringify(obj0))
  .add("0#Array ", () => JSON.stringify(arr0))
  .add("0#Custom", () => prefix + dateString + "," + level + "," + (source0 == null ? "null," : "[" + fastEscape(source0.file) + "," + source0.row + "," + source0.col + "],") + fastEscape(msgString) + (payload !== void 0 ? "," + JSON.stringify(payload) + "]" : "]"))
  .add("1#Object", () => JSON.stringify(obj1))
  .add("1#Array ", () => JSON.stringify(arr1))
  .add("1#Custom", () => prefix + dateString + "," + level + "," + (source1 == null ? "null," : "[" + fastEscape(source1.file) + "," + source1.row + "," + source1.col + "],") + fastEscape(msgString) + (payload !== void 0 ? "," + JSON.stringify(payload) + "]" : "]"))
  .add("2#Object", () => JSON.stringify(obj2))
  .add("2#Array ", () => JSON.stringify(arr2))
  .add("2#Custom", () => prefix + dateString + "," + level + "," + (source2 == null ? "null," : "[" + fastEscape(source2.file) + "," + source2.row + "," + source2.col + "],") + fastEscape(msgString) + (payload2 !== void 0 ? "," + JSON.stringify(payload2) + "]" : "]"))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  #0 (source + payload)
    1,540K Custom
      760K Array
      670K Object
  #1 (payload)
    1,810K Custom
    1,000K Array
      830K Object
  #2 (simple)
    20.5M Custom
     1.4M Array
     1.1M Object
*/

function fastEscape(str) {
  let result = "";
  let someEscape = false;
  let lastIdx = 0;
  let _len = str.length;
  for (let i = 0; i < _len; i++) {
    const code = str.charCodeAt(i);
    if (code === 34 || code === 92) {
      result += str.slice(lastIdx, i) + "\\";
      lastIdx = i;
      someEscape = true;
    } else if (code < 32) {
      result += str.slice(lastIdx, i);
      lastIdx = i + 1;
    }
  }
  return someEscape ? result + str.slice(lastIdx) : str;
}
