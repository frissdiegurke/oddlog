"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("array-clone-large");

let array = [];

for (let i = 0; i < 1000; i++) { array.push(i); }

suiteOddLog
  .add("Iterate", () => {
    let clone = [];
    let _len = array.length;
    for (let i = 0; i < _len; i++) { clone.push(array[i]); }
  })
  .add("Slice  ", () => array.slice())
  .add("Map    ", () => array.map(identity))
  .add("Concat ", () => [].concat(array))
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

function identity(value) { return value; }

/*
node@10.7.0

  926K Slice
  642K Concat
  413K Map
  199K Iterate
*/
