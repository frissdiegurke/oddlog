"use strict";

const Benchmark = require("benchmark");

const suiteOddLog = new Benchmark.Suite("iteration-small");

let set = new Set();
let array = [];
let object = {};

for (let i = 0; i < 5; i++) {
  set.add(i);
  array.push(i);
  object[i] = null;
}

suiteOddLog
  .add("Array#of", () => { for (let val of array) { val.toString(); } })
  .add("Array#i ", () => {
    let _len = array.length;
    for (let i = 0; i < _len; i++) {
      let item = array[i];
      item.toString();
    }
  })
  .add("Set     ", () => { for (let val of set) { val.toString(); } })
  .add("Object  ", () => { for (let key in object) { key.toString(); } })
  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

// Object won ~ factor 1.15 better than Set
// Set 2nd ~ factor 1.5 better than Array#i
// Update node@10.6.0: Object lost (factor 4); Array and Set draw
