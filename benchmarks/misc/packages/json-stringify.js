/* eslint max-len: "off" */
"use strict";

const Benchmark = require("benchmark");
const fastSafe = require("fast-safe-stringify");

const suiteOddLog = new Benchmark.Suite("json");

let payload0 = {package: "oddlog-benchmarks@0.1.0", production: false};
let payload1 = {some: {deeply: {nested: "values"}}, answer: [42]};
let payload2 = null;

suiteOddLog
  .add("a.Stringify ", () => JSON.stringify(payload0))
  .add("a.FastSafe  ", () => fastSafe(payload0))

  .add("b.Stringify ", () => JSON.stringify(payload1))
  .add("b.FastSafe  ", () => fastSafe(payload1))

  .add("c.Stringify ", () => JSON.stringify(payload2))
  .add("c.FastSafe  ", () => fastSafe(payload2))

  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  a)
    2.1M Stringify
    1.7M FastSafe
  b)
    1.3M Stringify
    1.0M FastSafe
  c)
    6.2M FastSafe
    6.0M Stringify
*/
