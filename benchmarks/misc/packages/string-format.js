/* eslint max-len: "off" */
"use strict";

const util = require("util");
const Benchmark = require("benchmark");
const quickFormat = require("quick-format");

const suiteOddLog = new Benchmark.Suite("json");

let msg0 = "application %s startup %d";
let msg1 = "application %j";
let arg00 = "server", arg01 = 42;
let arg1 = {some: {nested: {attributes: [21]}}, hello: "world"};
let optionsLow = {lowres: true};

console.log(util.format(msg0, arg00, arg01));
console.log(quickFormat([msg0, arg00, arg01]));
console.log(quickFormat([msg0, arg00, arg01], optionsLow));
console.log();
console.log(util.format(msg1, arg1));
console.log(quickFormat([msg1, arg1]));
console.log(quickFormat([msg1, arg1], optionsLow));

suiteOddLog
  .add("a.Util     ", () => util.format(msg0, arg00, arg01))
  .add("a.quick    ", () => quickFormat([msg0, arg00, arg01]))
  .add("a.quickLow ", () => quickFormat([msg0, arg00, arg01], optionsLow))

  .add("b.Util     ", () => util.format(msg1, arg1))
  .add("b.quick    ", () => quickFormat([msg1, arg1]))
  .add("b.quickLow ", () => quickFormat([msg1, arg1], optionsLow))

  // add listeners
  .on("error", err => console.error(err))
  .on("cycle", event => console.log(String(event.target)))
  // run async
  .run({async: true});

/*
node@10.7.0

  a)
    6.4M Util
    2.6M quick
    2.6M quickLow
  b)
    1.2M Util
    737K quickLow
    539K quick
*/
