"use strict";

const stream = require("stream");

module.exports = class DummyWriteStream extends stream.Writable {

  constructor(instant) {
    super();
    this.instant = instant !== false;
  }

  // noinspection JSMethodCanBeStatic
  _write(chunk, encoding, done) { this.instant ? done() : process.nextTick(done); }

  // noinspection JSMethodCanBeStatic
  _writev(chunks, callback) { callback(null); }

};
