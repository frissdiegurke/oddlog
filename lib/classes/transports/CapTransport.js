import NestedTransport from "./inherits/NestedTransport";
import BasicTransport from "./inherits/BasicTransport";
import {getLevelValue} from "../../services/levels";
import {MAPPED as LEVELS} from "../../constants/std-levels";

const __neglect = BasicTransport.prototype._neglect;

/*===================================================== Classes  =====================================================*/

/**
 * A NestedTransport class that filters messages above a given level.
 *
 * @class
 */
class CapTransport extends BasicTransport {

  /**
   * Creates a new CapTransport for the given logger with the given data scope.
   *
   * @param {!Object} options The options to pass to {@link BasicTransport} and {@link NestedTransport}.
   * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
   * @param {Number} cap The maximum message level to pass messages to children.
   * @param {Boolean} [createTransports] Passed to {@link NestedTransport}.
   * @constructor
   */
  constructor(options, createTransport, cap, createTransports) {
    super(options, LEVELS.silent);
    NestedTransport.call(this, options, createTransport, createTransports, false, null);
    this.cap = cap;
  }

  /**
   * Creates a child CapTransport instance.
   *
   * @param {Boolean} hasChildScope Whether the childLogger has a dedicated LoggerScope.
   * @returns {CapTransport} The child Transport.
   */
  child(hasChildScope) {
    const child = new CapTransport(this._options, this._createTransport, this.cap, false);
    child.postChild(this, hasChildScope);
    return child;
  }

  /**
   * Forwards the log to nested transports if its level is not above the cap level.
   *
   * @param {Log} log The log to process.
   * @see CapTransport#_neglect
   */
  write(log) { if (!this._neglect(log)) { this._write(log); } }

  /**
   * In addition to {@link BasicTransport#_neglect}, logs are ignored if their level is above the cap.
   *
   * @param {Log} log The log instance to check.
   * @returns {Boolean} Whether the log is to be ignored.
   * @private
   */
  _neglect(log) { return __neglect.call(this, log) || (log._useLevel && log.level > this.cap); }

}

CapTransport.prototype.attach = NestedTransport.prototype.attach;
CapTransport.prototype.minLevel = NestedTransport.prototype.minLevel;
CapTransport.prototype._write = NestedTransport.prototype.write;
CapTransport.prototype.postChild = NestedTransport.prototype.postChild;
CapTransport.prototype.forceChild = NestedTransport.prototype.forceChild;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new CapTransport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{Number|String} [cap=FATAL]` The maximum level allowed in order to forward a message to children.</li>
 *   <li>Additional options as accepted by {@link NestedTransport}.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @returns {CapTransport} The created transport.
 */
function create(options, createTransport) {
  const cap = getLevelValue(options.cap, LEVELS.fatal);

  return new CapTransport(options, createTransport, cap, true);
}

/*===================================================== Exports  =====================================================*/

export default CapTransport;
export {create};
