import BasicTransport from "./inherits/BasicTransport";
import DeferredTransport from "./inherits/DeferredTransport";

/*==================================================== Functions  ====================================================*/

class MemoryTransport extends BasicTransport {

  /**
   * Creates a new MemoryTransport with provided basic options for basic transports.
   *
   * @param {!Object} options The options to pass to {@link BasicTransport} and {@link DeferredTransport}.
   * @param {{push:Function}} store The in-memory store to add messages to.
   * @constructor
   */
  constructor(options, store) {
    super(options);
    DeferredTransport.call(this, options);
    this.store = store;
  }

  /**
   * Creates a child MemoryTransport instance.
   *
   * @returns {MemoryTransport} The child Transport.
   */
  child() { return new MemoryTransport(this._options, this.store); }

  /**
   * Appends the passed log to the store if it's not to be ignored.
   *
   * @param {Log} log The log to process.
   * @see BasicTransport#_neglect
   */
  write(log) {
    if (this._neglect(log)) { return; }
    this.preWrite(log);
    this.store.push(log);
  }

}

MemoryTransport.prototype.preWrite = DeferredTransport.prototype.preWrite;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new in-memory Transport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{?Array|{push:Function}} [store=[]]` The store to push messages to.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 *   <li>Additional options as accepted by {@link DeferredTransport}.</li>
 * </ul>
 * @returns {MemoryTransport} The created transport.
 */
function create(options) {
  let store = options.store || [];
  return new MemoryTransport(options, store);
}

/*===================================================== Exports  =====================================================*/

export default MemoryTransport;
export {create};
