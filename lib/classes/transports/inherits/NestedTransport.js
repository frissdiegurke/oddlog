import BasicTransport from "./BasicTransport";
import {create as createVoidTransport} from "../VoidTransport";

/*===================================================== Exports  =====================================================*/

/**
 * A Transport class abstraction that manages nested Transports.
 *
 * Implementations are expected to call {@link NestedTransport#postChild} at the end of `Transport#child()`.
 * Implementations are expected to pass `true` for `createTransports` only on root logger creation, not within child
 * creation.
 *
 * @class
 */
export default NestedTransport;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new NestedTransport with given options for nested transports.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{(?Object)[]} [transports]` A list of options to derive nested transports of.</li>
 * </ul>
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @param {Boolean} [createTransports=false] If true, create transports according to options.transports.
 * @param {Boolean} [inOrder=false] If true, `null` transport definitions are fit with void transport to make transports
 *        indices match their definitions.
 * @param {Number|String} [fallbackLevel] If provided, transport options that don't hold a level attribute, get assigned
 *        this as level.
 * @constructor
 */
function NestedTransport(options, createTransport, createTransports, inOrder, fallbackLevel) {
  this._createTransport = createTransport;
  const transports = options.transports;
  this.transports = [];

  if (createTransports && transports) {
    let _len = transports.length;
    for (let i = 0; i < _len; i++) {
      let transportDef = transports[i];
      if (transportDef == null) {
        if (inOrder) { this.transports.push(createVoidTransport()); }
        continue;
      }
      if (fallbackLevel != null && !transportDef.hasOwnProperty("level")) { transportDef.level = fallbackLevel; }
      this.transports.push(createTransport(transportDef));
    }
  }
}

/**
 * Adopts children of nested transports from parent transport. Needs to be called after the creation of child
 * Transports.
 *
 * @param {NestedTransport} parent The parent transport to derive nested transports from.
 * @param {Boolean} hasChildScope Whether the logger has a dedicated LoggerScope.
 * @returns {NestedTransport} Identity.
 */
NestedTransport.prototype.postChild = function (parent, hasChildScope) {
  const parentTransports = parent.transports, _len = parentTransports.length;
  const transports = this.transports;
  for (let i = 0; i < _len; i++) {
    let transport = parentTransports[i];
    if (hasChildScope || (typeof transport.forceChild === "function" ? transport.forceChild() : transport.forceChild)) {
      transport = transport.child(hasChildScope);
    }
    transports.push(transport);
  }
  return this;
};

/**
 * Returns true iff any of the nested transports demands a child.
 *
 * @returns {Boolean} Whether any nested transport demands a child.
 */
NestedTransport.prototype.forceChild = function () {
  const transports = this.transports, _len = transports.length;
  for (let i = 0; i < _len; i++) {
    const transport = transports[i];
    if (typeof transport.forceChild === "function" ? transport.forceChild() : transport.forceChild) { return true; }
  }
  return false;
};

/**
 * Triggers a write for all nested transports.
 *
 * @param {*} args The arguments to pass.
 */
NestedTransport.prototype.write = function (...args) {
  for (let transport of this.transports) { transport.write(...args); }
};

/**
 * Attaches all children of this transport to the passed logger.
 *
 * @param {Logger} logger The Logger to attach to.
 */
NestedTransport.prototype.attach = function (logger) {
  BasicTransport.prototype.attach.call(this, logger);
  for (let transport of this.transports) { if (typeof transport.attach === "function") { transport.attach(logger); } }
};

/**
 * Returns the minimum level of common logs to be processed by this transport.
 * For this transport type, this will return `max(this.level, min(level of nested transports))`.
 *
 * @returns {Number} The minimum level of logs to be processed by this transport.
 */
NestedTransport.prototype.minLevel = function () {
  let minLevel = Number.POSITIVE_INFINITY;
  for (let transport of this.transports) {
    const tMinLevel = transport.minLevel();
    if (minLevel > tMinLevel) { minLevel = tMinLevel; }
  }
  return minLevel > this.level ? minLevel : this.level;
};
