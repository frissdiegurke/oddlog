let instance = null;

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that does nothing.
 *
 * @class
 */
class VoidTransport {

  attach() {}

  // noinspection JSMethodCanBeStatic
  minLevel() { return Number.POSITIVE_INFINITY; }

  child() { return this; }

  write() {}

}

/*==================================================== Functions  ====================================================*/

/**
 * Returns a singleton VoidTransport.
 *
 * @returns {VoidTransport} The singleton VoidTransport.
 */
function create() {
  if (instance == null) { instance = new VoidTransport(); }
  return instance;
}

/*===================================================== Exports  =====================================================*/

export default VoidTransport;
export {create};
