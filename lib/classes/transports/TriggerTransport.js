import Log from "../Log";
import DeferredTransport from "./inherits/DeferredTransport";
import NestedTransport from "./inherits/NestedTransport";
import BasicTransport from "./inherits/BasicTransport";
import {MAPPED as LEVELS} from "../../constants/std-levels";
import {getLevelValue} from "../../services/levels";
import {assign} from "../../utils/object";
import {RayCacheScope, RingCacheScope} from "../CacheScope";

const DEFAULT_LIMIT = 50;

let floodingId = 0;

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that stores messages in a queue until a trigger threshold level is satisfied. In that case, all
 * cached messages are forwarded to nested transports.
 *
 * @class
 */
class TriggerTransport extends BasicTransport {

  /**
   * Creates a new TriggerTransport for the given logger with the given data scope.
   *
   * @param {!Object} options The options to pass to {@link BasicTransport} and {@link NestedTransport}.
   * @param {Number} threshold The trigger threshold level needed to initiate a cache flooding.
   * @param {Boolean} guard Whether to wrap floods with guarding logs.
   * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
   * @param {RingCacheScope|RayCacheScope} cacheScope The cache scope to use.
   * @param {Number|Boolean} [replicate] Whether to create dedicated caches for children. If a number is provided,
   *        children of higher nesting share the cache with their ancestors.
   * @param {Boolean} [createTransports] Passed to {@link NestedTransport}.
   * @constructor
   */
  constructor(options, threshold, guard, createTransport, cacheScope, replicate, createTransports) {
    super(options, LEVELS.silent);
    this._options = options;
    this.threshold = threshold;
    this.guard = guard;
    NestedTransport.call(this, options, createTransport, createTransports, false, null);
    DeferredTransport.call(this, this._options);
    this.forceChild = true;
    this.replicate = replicate;
    this._scope = cacheScope;
  }

  /**
   * Creates a child TriggerTransport instance.
   *
   * @param {Boolean} hasChildScope Whether the childLogger has a dedicated LoggerScope.
   * @returns {TriggerTransport} The child Transport.
   */
  child(hasChildScope) {
    if (!hasChildScope && (this.replicate === false)) { return this; }
    const replicate = typeof this.replicate !== "number" ? this.replicate :
      this.replicate >= 1 ? this.replicate - 1 : false;
    const scope = replicate === false ? this._scope : this._scope.child();
    let child = new TriggerTransport(
      this._options, this.threshold, this.guard, this._createTransport, scope, replicate, false
    );
    return child.postChild(this, hasChildScope);
  }

  /**
   * Processes a log if it is not to be ignored. If the log level is above the trigger threshold level of the Transport,
   * it triggers a flooding procedure {@link TriggerTransport#_trigger}. Otherwise, the log gets cached.
   *
   * @param {Log} log The log to handle.
   */
  write(log) {
    if (this._neglect(log)) { return; }
    if (log.level < this.threshold) {
      // not triggered; add to cache
      this._scope.push(this.preWrite(log));
    } else {
      // triggered; flood cache
      this._trigger({reason: "Trigger threshold satisfied.", level: log.level}, log);
    }
  }

  /**
   * Initiates a flooding procedure. This will write all cached logs into all nested Transports. It will also create
   * trace records to surround the block of cached logs.
   *
   * @param {Object} payload Additional payload to add to the trace records.
   * @param {Log} log The current log that triggered the flood.
   * @private
   */
  _trigger(payload, log) {
    const logger = this.logger;
    const id = floodingId++;

    logger.emit("transport:trigger:flooding:start", id, payload, this);

    // write pre-flood log
    if (this.guard) {
      const payloadStart = assign({id}, payload);
      this._write(new Log(logger, LEVELS.trace, null, [true, payloadStart, "--- start cache ---"], false));
    }

    // flood cached logs
    const payloadEnd = this._scope.flood(log, this._write.bind(this));

    // create post-flood log
    if (this.guard) {
      payloadEnd.id = id;
      assign(payloadEnd, payload);
      this._write(new Log(logger, LEVELS.trace, null, [true, payloadEnd, "--- end cache ---"], false));
    }

    logger.emit("transport:trigger:flooding:end", id, payload, this);
  }

}

TriggerTransport.prototype.preWrite = DeferredTransport.prototype.preWrite;
TriggerTransport.prototype.minLevel = NestedTransport.prototype.minLevel;
TriggerTransport.prototype._write = NestedTransport.prototype.write;
TriggerTransport.prototype.attach = NestedTransport.prototype.attach;
TriggerTransport.prototype.postChild = NestedTransport.prototype.postChild;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new TriggerTransport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{Number|String} [threshold=ERROR]` The trigger threshold level needed to initiate a cache flood.</li>
 *   <li>`{Boolean} [guard=true]` Whether to wrap the cached logs with guard logs (`--- start/end cache ---`).</li>
 *   <li>`{Number} [limit=50]` The cache size.</li>
 *   <li>`{Boolean|Number} [window=(limit >= 4096)]` If `false`, a pre-allocated array with the `limit` as length is
 *       used with performance-optimized operations. Otherwise it determines the amount of additional cache size for
 *       discarding multiple logs at once; `true` is interpreted as `max(5, limit/20)`.</li>
 *   <li>`{Boolean|Number} [replicate=false]` Whether to create dedicated caches for children. If a number is
 *       provided, children of higher nesting share the cache with their ancestors.</li>
 *   <li>`{Boolean} [silentWindow=false]` Affects only if `window !== false`. Whether to consider the cache discard
 *       window as already discarded and skip during flood.</li>
 *   <li>`{Number} [malloc]` Affects only if `window !== false`. The size of the initial cache array.</li>
 *   <li>`{Boolean} [cleanUp=true]` Whether to clean up the cache after each flood.</li>
 *   <li>`{Boolean} [freeUp=cleanUp]` Affects only if `window === false` and `cleanUp === true`. Whether to free up the
 *       cache for garbage collection after each flood. Disable for extreme performance when the cache would get
 *       overwritten shortly after flood anyways.</li>
 *   <li>Additional options as accepted by {@link NestedTransport}.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @param {Function} createTransport A function to create new Transport instances from transport definition objects.
 * @returns {TriggerTransport} The created transport.
 */
function create(options, createTransport) {
  const threshold = getLevelValue(options.threshold, LEVELS.error);
  const guard = options.guard !== false;
  const replicate = options.replicate || false;
  // create CacheScope variant
  const limit = typeof options.limit === "number" ? options.limit : DEFAULT_LIMIT;
  const cleanUp = options.cleanUp !== false;
  const window = options.hasOwnProperty("window") ? options.window : limit >= 4096;
  let scope;
  if (window === false) {
    const freeUp = cleanUp && (options.hasOwnProperty("freeUp") ? options.freeUp : true);
    scope = new RingCacheScope(limit, cleanUp, freeUp);
  } else {
    const windowSize = window === true ? (limit > 100 ? (limit / 20) | 0 : 5) : window;
    const silentWindow = !!options.silentWindow;
    scope = new RayCacheScope(limit, cleanUp, options.malloc, windowSize, silentWindow);
  }
  // create new transport
  return new TriggerTransport(options, threshold, guard, createTransport, scope, replicate, true);
}

/*===================================================== Exports  =====================================================*/

export default TriggerTransport;
export {create};
