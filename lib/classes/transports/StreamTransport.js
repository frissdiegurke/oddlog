import {EOL} from "os";

import BasicTransport, {isShy} from "./inherits/BasicTransport";
import {isAnySetAndTest as isEnvMatching} from "../../services/env";

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that writes messages to a stream.
 *
 * @class
 */
class StreamTransport extends BasicTransport {

  /**
   * Creates a new StreamTransport with the given options.
   *
   * @param {!Object} options The options to pass to {@link BasicTransport}.
   * @param {Stream} stream The stream to write messages to.
   * @param {Boolean} closeStream Whether to close the stream on LoggerScope#close.
   * @param {?String|Function} format The message format to use; see {@link Log#getRecordString}.
   * @param {?String} separator The separator to append to each message. Defaults to `EOL`.
   * @param {?String} encoding The encoding to use for the stream.
   * @constructor
   */
  constructor(options, stream, closeStream, format, separator, encoding) {
    super(options);
    this._options = options;
    this.stream = stream;
    this.closeStream = closeStream;
    this.separator = separator;
    this._format = format;
    this._encoding = encoding;

    this.writeCb = () => this.loggerScope.up();

    if (this._encoding != null) { this.stream.setDefaultEncoding(this._encoding); }
  }

  /**
   * Attaches this transport to the passed logger.
   *
   * @param {Logger} logger The logger to attach to.
   * @see BasicTransport#attach
   */
  attach(logger) {
    BasicTransport.prototype.attach.call(this, logger);
    const name = this.loggerScope.name;
    if (this._format != null) {
      this.format = this._format;
    } else {
      this.format = isShy(this) && !isEnvMatching(name) ? "simple" : "raw";
    }
    if (this.closeStream) { this.loggerScope.once("close", () => this._endStream()); }
  }

  /**
   * Creates a child StreamTransport instance.
   *
   * @returns {StreamTransport} The child Transport.
   */
  child() {
    return new StreamTransport(this._options, this.stream, false, this._format, this.separator, this._encoding);
  }

  /**
   * Writes the record of the given log and the separator to the stream.
   *
   * @param {Log} log The log to write.
   */
  write(log) {
    if (this._neglect(log)) { return; }
    this.loggerScope.down();
    this.stream.write(log.getRecordString(this.format) + this.separator, this.writeCb);
  }

  /**
   * End the underlying stream.
   * @private
   */
  _endStream() {
    this.loggerScope.down();
    this.stream.end(this.writeCb);
  }

}

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new StreamTransport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{Stream} [stream=process.stdout]` The stream to write messages to.</li>
 *   <li>`{Boolean} [closeStream=false]` Whether to close the stream when the logger gets closed.</li>
 *   <li>`{String|Function} [format="raw"]` The message format to use; see {@link Log#getRecordString}. Defaults to
 *       "simple" iff logger is set to be `shy` and neither DEBUG nor TRACE environment variables apply.
 *   <li>`{String} [separator=os.EOL]` The separator to add to messages.</li>
 *   <li>`{?String} [encoding]` The encoding to use.</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @returns {StreamTransport} The created transport.
 */
function create(options) {
  const stream = options.stream || process.stdout;
  const closeStream = options.closeStream;
  const format = options.format;
  const separator = options.hasOwnProperty("separator") ? options.separator : EOL;
  const encoding = options.encoding;

  return new StreamTransport(options, stream, closeStream, format, separator, encoding);
}

/*===================================================== Exports  =====================================================*/

export default StreamTransport;
export {create};
