/* eslint max-lines: ["warn", {"max": 300, "skipComments": true, "skipBlankLines": true}] */
import {EOL} from "os";
import path from "path";

import BasicTransport from "./inherits/BasicTransport";
import DeferredTransport from "./inherits/DeferredTransport";
import FileRotateScope from "../FileRotateScope";
import {sizeToBytes, timeToMillis} from "../../services/units";
import {rotate} from "../../services/file-rotate";

/*===================================================== Classes  =====================================================*/

/**
 * A Transport class that streams messages into a file and rotates the files given certain limits in size or time.
 *
 * @class
 */
class FileRotateTransport extends BasicTransport {

  /**
   * Creates a new FileRotateTransport for the given logger with the given data scope.
   *
   * @param {!Object} options The options to pass to {@link BasicTransport} and {@link DeferredTransport}.
   * @param {Boolean} closeStream Whether to close the stream when the logger gets closed.
   * @param {Object} fileRotateLoggerScope The rotation scope to use.
   * @constructor
   */
  constructor(options, closeStream, fileRotateLoggerScope) {
    super(options);
    DeferredTransport.call(this, options);
    this._options = options;
    this.closeStream = closeStream;

    this._scope = fileRotateLoggerScope;
  }

  /**
   * Attaches this transport to the passed logger.
   *
   * @param {Logger} logger The logger to attach to.
   * @see BasicTransport#attach
   */
  attach(logger) {
    if (this.logger != null) { throw new Error("Re-assignment of FileRotateTransport not supported."); }
    BasicTransport.prototype.attach.call(this, logger);
    const loggerScope = this.loggerScope;

    this.writeCb = () => loggerScope.up();
    this.errCb = (err) => logger._error(err);

    if (this.closeStream) { loggerScope.once("close", () => this._endStream()); }

    const active = this._scope.active;
    process.nextTick(() => logger.emit("transport:file-rotate:cork", active, null, this));
    active.open((err) => {
      if (err != null) { return this._handleFatal(err); }
      active.stream.on("error", this.errCb);
      logger.emit("transport:file-rotate:uncork", null, this);
      this._persistCache(active);
    });
  }

  /**
   * Creates a child FileRotateTransport instance.
   *
   * @returns {FileRotateTransport} The child Transport.
   */
  child() { return new FileRotateTransport(this._options, false, this._scope); }

  /**
   * Writes the given message plus message separator into the active file; Initiates a rotation beforehand if limits
   * were reached.
   *
   * @param {Log} log The log to write.
   * @param {Function} cb The callback.
   */
  write(log, cb) {
    if (this._neglect(log)) { return; }
    const scope = this._scope;
    const active = scope.active;
    if (active.ready && !scope.queueRotation) {
      // we need to create a Buffer to be able to predict the actual size on disk
      const recordBuffer = Buffer.from(log.getRawRecordString() + scope.separator, scope.encoding);
      const recordSize = recordBuffer.length;
      // check for rotation necessity
      if (active.shouldRotate(log.date, recordSize)) {
        this._rotate();
      } else {
        this._write(active, recordBuffer, recordSize, cb);
        return;
      }
    }
    // if no fatal occurred, add message to cache in order to flush after new file stream got opened
    if (scope.cache !== null) {
      this.preWrite(log);
      this.loggerScope.down();
      scope.cache.push({transport: this, message: log, cb});
    }
  }

  _rotate() {
    const scope = this._scope;
    if (scope.rotating) {
      // postpone rotation til active rotation is done
      scope.queueRotation = true;
      return;
    }
    scope.rotating = true;
    const oldScope = scope.active;
    if (oldScope.stream !== null && !oldScope.failed) {
      if (oldScope.ready) {
        oldScope.stream.end();
      } else {
        oldScope.once("ready", () => oldScope.stream.end());
      }
    }
    const active = scope.active = new FileRotateScope(this._getPath(), scope.encoding, scope.limits);
    const logger = this.logger;
    logger.emit("transport:file-rotate:cork", active, oldScope, this);
    active.open((err) => {
      if (err != null) { return this._handleFatal(err); }
      active.stream.on("error", this.errCb);
      logger.emit("transport:file-rotate:uncork", null, this);
      this._persistCache(active);
      // new file stream is opened; initiate remove/rename/compress of files
      logger.emit("transport:file-rotate:rotation:start", this);
      rotate(
        scope.pathDir, scope.pathFile, path.basename(active.file), scope.maxFiles, scope.compress,
        (err, scopeFile) => {
          active.file = scopeFile;
          this._afterRotate(err);
        }
      );
    });
  }

  _getPath(num) {
    const scope = this._scope;
    if (num === 0) { return scope.path; }
    if (num > 0) { return scope.path + "." + num; }
    return path.join(scope.pathDir, "." + scope.pathFile); // temporary file, should get rotated to `scope.path` ASAP
  }

  _afterRotate(err) {
    const scope = this._scope;
    if (err != null) { this.logger._error(err); }
    scope.rotating = false;
    this.logger.emit("transport:file-rotate:rotation:end", err, this);
    if (scope.queueRotation) {
      scope.queueRotation = false;
      this._rotate();
    }
  }

  _write(active, messageBuffer, messageSize, cb) {
    if (typeof cb === "function") {
      const _cb = cb;
      cb = () => {
        this.loggerScope.up();
        _cb();
      };
    } else {
      cb = this.writeCb;
    }
    this.loggerScope.down();
    active.size += messageSize;
    active.stream.write(messageBuffer, cb);
  }

  _persistCache(active) {
    const scope = this._scope;
    if (active !== scope.active) { return; }
    const cache = scope.cache;
    if (cache !== null && cache.length > 0) {
      scope.cache = [];
      // keep track of amount of items per loggerScope to bulk-update the LoggerScope#up calls
      const scopeMap = new Map(), scopeList = [];
      const _len = cache.length;
      for (let i = 0; i < _len; i++) {
        const data = cache[i];
        const loggerScope = data.transport.loggerScope;
        if (scopeMap.has(loggerScope)) {
          scopeMap.set(loggerScope, scopeMap.get(loggerScope) + 1);
        } else {
          scopeMap.set(loggerScope, 1);
          scopeList.push(loggerScope);
        }
        data.transport.write(data.message, data.cb);
      }
      // messages have called LoggerScope#down synchronously within FileRotateTransport#write again, so nullify once
      for (let i = 0; i < scopeList.length; i++) {
        const loggerScope = scopeList[i];
        loggerScope.upTimes(scopeMap.get(loggerScope));
      }
    }
  }

  _handleFatal(err) {
    this.logger.emit("transport:file-rotate:uncork", err, this);
    this.logger._error(err);
    this._blockTransport();
    throw err;
  }

  _endStream() {
    const scope = this._scope;
    if (typeof scope.active.endStream === "function") {
      this.loggerScope.down();
      scope.active.endStream(this.writeCb);
    }
    this._blockTransport();
  }

  _blockTransport() {
    const scope = this._scope;
    scope.active = {ready: false};
    if (scope.cache !== null) {
      const cache = scope.cache;
      scope.cache = null;
      // messages have called LoggerScope#down synchronously within FileRotateTransport#write again, so nullify once
      // keep track of amount of items per loggerScope to bulk-update the LoggerScope#up calls
      const scopeMap = new Map(), scopeList = [];
      const _len = cache.length;
      for (let i = 0; i < _len; i++) {
        const data = cache[i];
        const loggerScope = data.transport.loggerScope;
        if (scopeMap.has(loggerScope)) {
          scopeMap.set(loggerScope, scopeMap.get(loggerScope) + 1);
        } else {
          scopeMap.set(loggerScope, 1);
          scopeList.push(loggerScope);
        }
      }
      for (let i = 0; i < scopeList.length; i++) {
        const loggerScope = scopeList[i];
        loggerScope.upTimes(scopeMap.get(loggerScope));
      }
    }
  }

}

FileRotateTransport.prototype.preWrite = DeferredTransport.prototype.preWrite;

/*==================================================== Functions  ====================================================*/

/**
 * Creates a new FileRotateTransport according to given options.
 *
 * @param {!Object} options The options:
 * <ul>
 *   <li>`{String} path` The path of the main file to log to (no rotation suffix).</li>
 *   <li>`{String} [separator=EOL]` The separator to add to messages.</li>
 *   <li>`{String} [encoding="utf8"]` The encoding to use.</li>
 *   <li>`{Number|String} [size]` The maximum size of one file.</li>
 *   <li>`{Number|String} [interval]` The maximum age of one file.</li>
 *   <li>`{Number} [maxFiles]` The maximum amount of files to keep.</li>
 *   <li>`{Boolean|Object} [compress]` Whether to compress rotated files using gzip. A provided object will be passed to
 *       `zlib.createGzip`.</li>
 *   <li>Additional options as accepted by {@link DeferredTransport} (messages are deferred if incoming while rotation
 *       is in action).</li>
 *   <li>Additional options as accepted by {@link BasicTransport}.</li>
 * </ul>
 * @returns {FileRotateTransport} The created transport.
 */
function create(options) {
  const file = options.path;
  const separator = options.hasOwnProperty("separator") ? options.separator : EOL;
  const encoding = options.encoding || "utf8";
  const limits = {
    maxSize: sizeToBytes(options.size),
    maxAge: timeToMillis(options.interval)
  };
  const maxFiles = options.maxFiles;
  const compress = options.compress;
  const active = new FileRotateScope(file, encoding, limits);

  const scope = {
    path: file, separator, encoding, limits, maxFiles, compress, active,
    pathDir: path.dirname(file), pathFile: path.basename(file),
    rotating: false, // blocks the initiation of another rotation at the same time
    queueRotation: false, // whether after the rotation is done, another rotation got triggered already
    cache: [] // gets filled during file-stream swap; gets set to null when a fatal occurs
  };

  return new FileRotateTransport(options, true, scope);
}

/*===================================================== Exports  =====================================================*/

export default FileRotateTransport;
export {create};
