/* eslint max-lines: ["warn", {"max": 500, "skipComments": true, "skipBlankLines": true}] */
import EventEmitter from "events";

import {LIST as LEVEL_LIST, MAPPED as LEVELS} from "../constants/std-levels";
import {nameToValue} from "../services/levels";
import {createTransport} from "../services/transports";
import {self as selfFn, stubFalse, stubTrue} from "../utils/function";
import {assign, assignWeak, clone} from "../utils/object";
import {getCallerPosition} from "../utils/misc";

import LoggerScope, {SCOPE_NAME_SEPARATOR} from "./LoggerScope";
import Log from "./Log";

const LOGGING_METHODS = [];
const LEVEL_CAP = LEVEL_LIST.length + 1;

/*===================================================== Classes  =====================================================*/

/**
 * The main class of the oddlog module. It manages transports and accepts message data to forward.
 *
 * The following Events are emitted:
 *
 *  * "error" {Error} error - Whenever an error occurs.
 *  * "log" {Log} log - Whenever logging methods are used.
 *  * "child" {Logger} child - Whenever a child logger got created.
 *
 * Additional events that regard children as well are emitted on {@link Logger#loggerScope}.
 *
 * For transport events, take a look at the docs.
 *
 * @class
 */
class Logger extends EventEmitter {

  /**
   * Creates a new Logger instance.
   * This function is verbose in arguments in order to have no computing going on here. This speeds up child creation,
   * but requires very specific argument conditions.
   *
   * @param {String} prefix The prefix of log record strings.
   * @param {?Object} [payload] The payload to inherit to all children and messages.
   * @param {?Object} [parentPayload] The already transformed payload of the parent.
   * @param {?Object} [transformer] A key/function mapping of transformer functions to call with payload values.
   * @param {Number} level The default level to fallback to for the {@link Logger#log} function.
   * @param {Boolean} shy Whether to enable some library-sane default options for transports.
   * @param {Boolean} ownChildPayloads Whether the payload passed to child and log functions is allowed to be modified
   *        by the logger. This can be overwritten on a per-child/-message basis.
   * @param {Boolean} singleTransform Whether transformations shall only be executed at message level. If set to
   *        false, logger level payload transformations will be cached.
   * @param {?Boolean} logSource Whether to add the source-code location of the calling function to log messages of
   *        child logger scopes.
   * @param {Function} messageFormatter The formatter function to use for primary message texts.
   * @param {LoggerScope} loggerScope The scope to use.
   * @param {Boolean} ownsLoggerScope Whether the instance is the root logger of the given LoggerScope.
   * @constructor
   */
  constructor(
    prefix, payload, parentPayload, transformer, level, shy, ownChildPayloads, singleTransform, logSource,
    messageFormatter, loggerScope, ownsLoggerScope
  ) {
    super();
    /**
     * This is the shared logger scope between a root logger instance and its children.
     * @type {LoggerScope}
     */
    this.loggerScope = loggerScope;
    this.transformer = transformer;
    this.transports = [];
    this.ownChildPayloads = ownChildPayloads;
    this.messageFormatter = messageFormatter;
    this._logSource = logSource;
    this._defaultLevel = level;
    this._level = level;
    this._prefix = prefix;
    this._payload = payload;
    this._shy = shy;
    this._singleTransform = singleTransform;
    this._parentPayload = parentPayload;
    this._ownsLoggerScope = ownsLoggerScope;

    this.__minTransportLevel = LEVEL_CAP;
    this.__lastLog = null;
    this.__mergedPayload = null;
    this.__hasMergedPayload = false;
    this.__processListeners = {};
  }

  /**
   * Creates a middleware function to be used by express.js compatible routers. The middleware attaches a child logger
   * to the request object (req.log).
   *
   * @param {Boolean} [ownPayload=this.ownChildPayloads] Whether the payload is allowed to be modified by the logger.
   * @param {Boolean} [ownChildPayloads=this.ownChildPayloads] Whether to keep the messages and children payload safe.
   *        If set to false, the payload of messages and children may get modified. This can be overwritten on child and
   *        log function calls.
   * @param {Boolean|String} [childScope=false] Whether to use a dedicated LoggerScope. Passed to {@link Logger#child}.
   * @param {?Object|Function<req,res>|Function<req,res,cb>} [payload={req,res}] Payload to add to the child. If a
   *        function is provided it is supposed to return the payload to use. It may operate async if it accepts the
   *        callback function (to be called with error and result parameter).
   * @returns {Function} The middleware for express.js compatible routers; Accepting request, response and callback.
   */
  mwExpress(ownPayload, ownChildPayloads, childScope, payload) {
    if (typeof ownPayload !== "boolean") {
      if (typeof ownPayload === "string") {
        childScope = ownPayload;
        payload = ownChildPayloads;
      } else {
        childScope = false;
        payload = ownPayload;
      }
      ownPayload = ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownChildPayloads !== "boolean") {
      if (typeof ownChildPayloads === "string") {
        payload = childScope;
        childScope = ownChildPayloads;
      } else {
        payload = ownChildPayloads;
        childScope = false;
      }
      ownChildPayloads = this.ownChildPayloads;
    } else if (typeof childScope !== "boolean" && typeof childScope !== "string") {
      payload = childScope;
      childScope = false;
    }
    if (payload === void 0) {
      return (req, res, next) => {
        req.log = this.child(ownPayload, ownChildPayloads, {req, res}, childScope);
        next();
      };
    } else if (typeof payload === "function") {
      if (payload.length > 2) {
        return (req, res, next) => {
          payload(req, res, (err, payload) => {
            req.log = this.child(ownPayload, ownChildPayloads, payload, childScope);
            next(err);
          });
        };
      } else {
        return (req, res, next) => {
          req.log = this.child(ownPayload, ownChildPayloads, payload(req, res), childScope);
          next();
        };
      }
    } else {
      return (req, res, next) => {
        req.log = this.child(ownPayload, ownChildPayloads, payload, childScope);
        next();
      };
    }
  }

  /**
   * Creates a new Transport from the given options (utilizing {@link createTransport}) and adds it to active transports
   * of this logger.
   *
   * @param {!Object} transportOptions The options to derive the Transport of.
   * @returns {Logger} Identity.
   */
  addTransport(transportOptions) {
    const transport = createTransport(transportOptions);
    if (transport == null) { return this; }
    if (typeof transport.attach === "function") { transport.attach(this); }
    this.transports.push(transport);
    this._updateMinTransportLevel(transport.minLevel());
    return this;
  }

  /**
   * Adds multiple transports as defined by the given options list.
   *
   * @param {(!Object)[]} transportOptionsList The list of options to derive each a Transport of.
   * @returns {Logger} Identity.
   * @see Logger#addTransport
   */
  addTransports(transportOptionsList) {
    let minLvl = this.__minTransportLevel, transports = this.transports, _len = transportOptionsList.length;
    for (let i = 0; i < _len; i++) {
      const transport = createTransport(transportOptionsList[i]);
      if (transport == null) { continue; }
      if (typeof transport.attach === "function") { transport.attach(this); }
      transports.push(transport);
      const tMinLevel = transport.minLevel();
      if (tMinLevel < minLvl) { minLvl = tMinLevel; }
    }
    this._updateMinTransportLevel(minLvl);
    return this;
  }

  /**
   * Adds the properties of given additional payload to the logger payload.
   *
   * @param {Boolean} [ownPayload=this.ownChildPayloads] Whether the payload is allowed to be modified by the logger.
   * @param {?Object} payload The additional payload to merge into logger payload.
   * @returns {Logger} Identity.
   */
  mergePayload(ownPayload, payload) {
    if (typeof ownPayload !== "boolean") {
      payload = ownPayload;
      ownPayload = this.ownChildPayloads;
    }
    if (payload === null) {
      this.__hasMergedPayload = true;
      this.__mergedPayload = null;
    } else if (payload !== void 0) {
      let transform = this.transformer != null && !this._singleTransform;
      if (this.__hasMergedPayload) {
        if (transform) {
          //noinspection JSCheckFunctionSignatures
          payload = this._transformPayload(ownPayload ? payload : clone(payload));
          ownPayload = true;
        }
        if (this.__mergedPayload == null) {
          this.__mergedPayload = ownPayload ? payload : clone(payload);
        } else {
          assign(this.__mergedPayload, payload);
        }
      } else {
        if (this._payload == null) {
          this._payload = ownPayload ? payload : clone(payload);
        } else {
          assign(this._payload, payload);
        }
      }
    }
    return this;
  }

  /**
   * Creates a new child logger, holding the additional payload.
   *
   * @param {Boolean} [ownPayload=this.ownChildPayloads] Whether the payload is allowed to be modified by the logger.
   * @param {Boolean} [ownChildPayloads=this.ownChildPayloads] Whether to keep the messages and children payload safe.
   *        If set to false, the payload of messages and children may get modified. This can be overwritten on child and
   *        log function calls.
   * @param {String|Boolean} [childScope=false] If set, use a dedicated LoggerScope with the value as name-suffix. This
   *        enables the use of {@link Logger#close} on the child and enables dedicated events like LoggerScope:drain.
   * @param {?Object} [payload] Payload to add to the child.
   * @returns {Logger} The child.
   */
  // eslint-disable-next-line complexity
  child(ownPayload, ownChildPayloads, childScope, payload) {
    if (typeof ownPayload === "object") {
      childScope = false;
      payload = ownPayload;
      ownPayload = ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownPayload === "string") {
      childScope = ownPayload;
      payload = ownChildPayloads;
      ownPayload = ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownChildPayloads === "object") {
      childScope = false;
      payload = ownChildPayloads;
      ownChildPayloads = this.ownChildPayloads;
    } else if (typeof ownChildPayloads === "string") {
      payload = childScope;
      childScope = ownChildPayloads;
      ownChildPayloads = this.ownChildPayloads;
    } else if (typeof payload === "boolean" || typeof payload === "string") {
      // noinspection JSValidateTypes
      childScope = payload;
      payload = void 0;
    }
    let hasChildScope = false, childScopeName = null;
    if (childScope === true) {
      hasChildScope = true;
      childScopeName = this.loggerScope.name;
    } else if (typeof childScope === "string") {
      hasChildScope = true;
      childScopeName = this.loggerScope.name;
      if (childScope.length) { childScopeName += SCOPE_NAME_SEPARATOR + childScope; }
    }
    if (!ownPayload && payload != null) { payload = clone(payload); }
    let childLoggerScope;
    if (hasChildScope) {
      childLoggerScope = new LoggerScope(childScopeName, this._logSource, this.loggerScope);
    } else {
      childLoggerScope = this.loggerScope;
    }
    let child = new Logger(
      this._prefix, payload, this.getPreparedPayload(), this.transformer, this._defaultLevel, this._shy,
      ownChildPayloads, this._singleTransform, this._logSource, this.messageFormatter, childLoggerScope, hasChildScope
    );
    let transports = child.transports;
    let parentTransports = this.transports, _len = parentTransports.length, transport;
    let minLvl = child.__minTransportLevel;
    // clone parent-transports list, transforming transports with their child-hooks
    for (let i = 0; i < _len; i++) {
      transport = parentTransports[i];
      if (childScope || (typeof transport.forceChild === "function" ? transport.forceChild() : transport.forceChild)) {
        transport = transport.child(childScope);
        if (typeof transport.attach === "function") { transport.attach(child); }
      }
      transports.push(transport);
      const tMinLevel = transport.minLevel();
      if (tMinLevel < minLvl) { minLvl = tMinLevel; }
    }
    child._updateMinTransportLevel(minLvl);
    this.emit("child", child);
    return child;
  }

  /**
   * Returns and caches the full payload of the logger merged into the parent payload. Iff singleTransform is set to
   * false, the merged payload will also be transformed.
   * The resulting object is not allowed to be modified any further. It could be the same object as the parents
   * (non-transformed) payload.
   *
   * @returns {?Object|undefined} The merged (and eventually transformed) payload.
   */
  getPreparedPayload() {
    if (this.__hasMergedPayload) { return this.__mergedPayload; }
    this.__hasMergedPayload = true;
    let inheritPayload = this._parentPayload, payload = this._payload;
    if (payload === void 0) { return this.__mergedPayload = inheritPayload; }
    if (payload === null) { return this.__mergedPayload = payload; }
    let transform = this.transformer != null && !this._singleTransform;
    if (inheritPayload == null) { return this.__mergedPayload = transform ? this._transformPayload(payload) : payload; }
    if (transform) { payload = this.__mergedPayload = this._transformPayload(payload); }
    return this.__mergedPayload = assignWeak(payload, inheritPayload);
  }

  /**
   * Performs a merge of the inherited payload with message payload. The inherited payload is assumed to already be
   * transformed iff singleTransform is false.
   *
   * @param {?Object} [inheritPayload] The parent payload payload.
   * @param {?Object} [payload] The message payload; Its values get transformed and may overwrite values of
   *        inheritPayload. If it is null, the result will be null as well.
   * @param {Boolean} ownPayload Whether the payload is allowed to be modified by the logger.
   * @returns {?Object|undefined} The transformed and merged payload; Undefined if both payloads are undefined.
   * @private
   */
  _getFinalPayload(inheritPayload, payload, ownPayload) {
    if (payload === void 0) {
      if (inheritPayload == null) { return inheritPayload; }
      if (this._singleTransform && this.transformer != null) {
        return this._transformPayload(clone(inheritPayload));
      }
      return inheritPayload;
    }
    if (payload === null) { return payload; }
    if (inheritPayload == null) {
      if (this.transformer != null) {
        //noinspection JSCheckFunctionSignatures
        return this._transformPayload(ownPayload ? payload : clone(payload));
      }
      return payload;
    }
    if (!ownPayload) { payload = clone(payload); }
    if (this.transformer != null) {
      if (this._singleTransform) {
        return this._transformPayload(assignWeak(payload, inheritPayload));
      } else {
        //noinspection JSCheckFunctionSignatures
        return assignWeak(this._transformPayload(payload), inheritPayload);
      }
    }
    return assignWeak(payload, inheritPayload);
  }

  /**
   * Runs available transformers on the given object.
   *
   * @param {!Object} obj The object to apply transformers to.
   * @returns {!Object} The given object.
   * @private
   */
  _transformPayload(obj) {
    let transformer = this.transformer;
    for (let key in transformer) {
      //noinspection JSUnfilteredForInLoop
      if (obj.hasOwnProperty(key)) {
        //noinspection JSUnfilteredForInLoop
        obj[key] = transformer[key](obj[key]);
      }
    }
    return obj;
  }

  /**
   * Sets the current active level to the given value.
   * This is useful in conjunction with {@link Logger#log}.
   *
   * @param {Number|String} level The level. It should match a valid {@link LEVELS} key if a String is provided.
   * @returns {Logger} Identity.
   * @see Logger#done
   * @see Logger#log
   * @see Logger#getLevel
   */
  setLevel(level) {
    this._level = level = typeof level === "string" ? nameToValue(level) : level >= 0 ? level : -1;
    this.log = level >= this.__minTransportLevel ? log : selfFn;
    return this;
  }

  /**
   * Returns the current active level.
   *
   * @returns {Number} The active level.
   * @see Logger#setLevel
   */
  getLevel() { return this._level; }

  /**
   * Resets the active level to default value or option used for constructor.
   * This is supposed to be called after {@link Logger#log} calls.
   *
   * @returns {Logger} Identity.
   * @see Logger#setLevel
   * @see Logger#log
   */
  done() {
    this._level = this._defaultLevel;
    return this;
  }

  /**
   * Closes the logger and all its children. Calls the given callback when all remaining messages are flushed to their
   * destination.
   *
   * A closed logger will no longer pass messages to its target streams.
   *
   * @param {Boolean} [failSilent=true] Whether to silently ignore future incoming messages. If false, an Error will be
   *        thrown on further message method calls.
   * @param {Function} [cb] The callback.
   * @throws {Error} The logger has already been closed.
   * @throws {Error} The logger is not the root but a child of its LoggerScope.
   * @returns {Logger} Identity.
   *
   * @see Logger#child
   */
  close(failSilent, cb) {
    let loggerScope = this.loggerScope;
    if (!this._ownsLoggerScope) {
      throw new Error(
        "The logger is a child without dedicated scope. It cannot be closed, use Logger#close on the root logger " +
        "instance instead."
      );
    }
    if (loggerScope.closed) { throw new Error("Logger has already been closed."); }
    if (typeof failSilent === "function") {
      //noinspection JSValidateTypes
      cb = failSilent;
      failSilent = true;
    }
    failSilent = failSilent !== false;
    // detach all listeners
    for (let key in this.__processListeners) {
      if (this.__processListeners.hasOwnProperty(key)) {
        let arr = this.__processListeners[key], _len = arr.length;
        for (let i = 0; i < _len; i++) { process.removeListener(key, arr[i]); }
      }
    }
    this.__processListeners = null;
    if (typeof cb === "function") { loggerScope.once("end", cb); }
    loggerScope._close(failSilent);
    // free up objects for garbage collection
    this.__lastLog = this.__mergedPayload = this._payload = null;
    this.transports = this.messageFormatter = this._parentPayload = null;
    return this;
  }

  /**
   * Registers an event handler to the `uncaughtException` event on the process.
   *
   * @param {String|Number} [level=FATAL] The level to use for the logs.
   * @param {Boolean|Number|Function} [exit=true] Whether to exit, the exit code or a function that returns either. If
   *        it's a function, it gets called with the error and might accept 2nd parameter callback function to operate
   *        async.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleProcessEvents
   */
  handleUncaughtExceptions(level, exit) {
    if (typeof level !== "string" && typeof level !== "number") {
      if (level != null) { exit = level; }
      level = LEVELS.fatal;
    }
    if (exit == null) { exit = stubTrue; }
    this.handleProcessEvents("uncaughtException", "Uncaught exception", level, exit, "err");
    return this;
  }

  /**
   * Registers an event handler to the `unhandledRejection` event on the process.
   *
   * @param {String|Number} [level=ERROR] The level to use for the logs.
   * @param {Boolean|Number|Function} [exit=false] Whether to exit, the exit code or a function that returns either. If
   *        it's a function, it gets called with the error and might accept 2nd parameter callback function to operate
   *        async.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleProcessEvents
   */
  handleUnhandledRejections(level, exit) {
    if (typeof level !== "string" && typeof level !== "number") {
      if (level != null) { exit = level; }
      level = LEVELS.error;
    }
    this.handleProcessEvents("unhandledRejection", "Unhandled rejection.", level, exit, "err");
    return this;
  }

  /**
   * Registers an event handler to the `warning` event on the process.
   *
   * @param {String|Number} [level=WARN] The level to use for the logs.
   * @param {Boolean|Number|Function} [exit=false] Whether to exit, the exit code or a function that returns either. If
   *        it's a function, it gets called with the error and might accept 2nd parameter callback function to operate
   *        async.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleProcessEvents
   */
  handleWarnings(level, exit) {
    if (typeof level !== "string" && typeof level !== "number") {
      if (level != null) { exit = level; }
      level = LEVELS.warn;
    }
    this.handleProcessEvents("warning", "Process warning.", level, exit);
    return this;
  }

  /**
   * Registers an event handler to the given event on `process` that logs logs the event data.
   *
   * @param {String|String[]} eventName The name of the event to watch for on the process.
   * @param {String|Function} message The message to log. If a function is provided it gets called with the event data
   *        (1st parameter) and the logger instance (2nd parameter); It is expected to return a String.
   * @param {String|Number} level The logging level.
   * @param {Boolean|Number|Function} [exit=false] Whether to exit the process after event occurred, the exit code or a
   *        function that returns either. If it's a function, it gets called with the error and might accept 2nd
   *        parameter callback function to operate async.
   * @param {?String|Object} [key] The payload key to use for the event data if any. If an object is given, it will be
   *        used as payload. If not specified, the event data will be used as payload.
   * @returns {Logger} Identity.
   *
   * @see Logger#handleUncaughtExceptions
   * @see Logger#handleUnhandledRejections
   * @see Logger#handleWarnings
   */
  handleProcessEvents(eventName, message, level, exit, key) {
    let self = this;
    if (self.loggerScope.closed) { throw new Error("Logger has already been closed."); }
    let source = self.loggerScope.logSource ? getCallerPosition(3) : null;
    let listeners = this.__processListeners;
    if (typeof exit === "string" || exit === null && arguments.length < 5) {
      //noinspection JSValidateTypes
      key = exit;
      exit = stubFalse;
    } else if (typeof exit !== "function") {
      let value = exit;
      exit = () => value;
    }
    level = typeof level === "string" ? nameToValue(level) : level >= 0 ? level : -1;

    if (typeof eventName === "string") {
      add(eventName);
    } else {
      for (let i = 0; i < eventName.length; i++) { add(eventName[i]); }
    }

    return this;

    function add(name) {
      if (listeners.hasOwnProperty(name)) { listeners[name].push(handler); } else { listeners[name] = [handler]; }
      process.on(name, handler);
    }

    function handler(data) {
      if (!self.loggerScope.closed) {
        // log generic message
        let text = typeof message === "function" ? message(data, self) : message;
        let payload = typeof key === "string" ? {[key]: data} : key === void 0 ? data : key;
        let args = payload === void 0 ? [true, text] : [true, payload, text];
        let msg = self.__lastLog = new Log(self, level, source, args, true);
        self.emit("message", msg);
        let transports = self.transports, _len = transports.length;
        for (let i = 0; i < _len; i++) { transports[i].write(msg); }
      }
      // handle exit procedure
      if (exit.length > 1) {
        exit(data, (value) => self.exit(typeof value === "number" ? value : !!value));
      } else {
        let value = exit(data);
        self.exit(typeof value === "number" ? value : !!value);
      }
    }
  }

  /**
   * Calls process.exit after the logger has been closed.
   *
   * @param {Number|Boolean} [code=0] The exit code to use. If false, exit will be omitted. True results in exit code 1.
   * @returns {Logger} Identity.
   *
   * @see Logger#close
   */
  exit(code) {
    let done;
    if (code == null || code === 0) {
      done = () => process.exit(0);
    } else if (typeof code === "number") {
      done = () => process.exit(code);
    } else if (typeof code === "boolean" && code) {
      done = () => process.exit(1);
    } else {
      return this;
    }
    if (this.loggerScope.closed) {
      if (this.loggerScope.busyWrites) { this.loggerScope.once("end", done); } else { done(); }
    } else {
      this.close(done);
    }
    return this;
  }

  /**
   * To be called after transports have been added.
   * Updates the logger instance to provide all necessary logging functions instead of pre-existing noop references.
   *
   * @param {Number} level The new minimum level to require actual logging functions.
   * @private
   */
  _updateMinTransportLevel(level) {
    if (this.__minTransportLevel <= level) { return; }
    let lvl, minLvl;
    // lvl = min(0, ceil(transport.minLevel()))
    if (level >= 0) {
      // fast bitwise ceil implementation
      minLvl = level;
      lvl = level << 0;
      lvl = lvl === level ? lvl : lvl + 1;
    } else {
      lvl = minLvl = 0;
    }
    let value = this.__minTransportLevel;
    while (value > lvl) {
      let name = LEVEL_LIST[--value];
      this[name] = LOGGING_METHODS[value];
    }
    this.log = this._level >= minLvl ? log : selfFn;
    this.__minTransportLevel = minLvl;
  }

  _error(err) { this.emit("error", err); }

}

Logger.prototype.log = selfFn;

for (let name in LEVELS) {
  //noinspection JSUnfilteredForInLoop
  let value = LEVELS[name], nameCapital = name[0].toUpperCase() + name.substring(1);
  //noinspection JSUnfilteredForInLoop
  Logger.prototype[name] = selfFn;

  /**
   * Logs a message with the level according to function name.
   *
   * @param {*} args The arguments (in this order):
   * <ul>
   *   <li>`{Boolean} [ownPayload]` Set to overwrite ownChildPayloads option of the logger.</li>
   *   <li>`{?Object} [payload]` Payload data to add to the message.</li>
   *   <li>`{String} [message]` String to use as primary message.</li>
   *   <li>`{String...|Object} [formatValues]` Additional arguments to format the primary message.</li>
   * </ul>
   * @returns {Logger} Identity.
   * @throws {Error} The logger got closed already and silent has been set to false ({@link Logger#close}).
   */
  LOGGING_METHODS[value] = function (...args) {
    if (this.loggerScope.closed) {
      let err = new Error("Received log record on closed Logger.");
      this.emit("log-error", {err, args});
      if (this.loggerScope._throw) { throw err; }
      return this;
    }
    let log = new Log(this, value, this.loggerScope.logSource ? getCallerPosition(3) : null, args, true);
    this.__lastLog = log;
    this.emit("log", log);
    let transports = this.transports, _len = transports.length;
    for (let i = 0; i < _len; i++) { transports[i].write(log); }
    return this;
  };

  /**
   * Sets the current active level according to function name. This is useful in conjunction with {@link Logger#log}.
   *
   * @returns {Logger} Identity.
   */
  Logger.prototype["set" + nameCapital] = function () {
    this._level = value;
    this.log = value >= this.__minTransportLevel ? log : selfFn;
    return this;
  };

  /**
   * Returns whether the minimum level to process logs is at or below the level within the method name.
   *
   * @returns {Boolean} Whether a message of this level would get processed.
   */
  Logger.prototype["is" + nameCapital] = function () {
    return this.__minTransportLevel <= value;
  };

}

/*==================================================== Functions  ====================================================*/

/**
 * Logs a message with the current active level.
 *
 * @param {*} args The arguments (in this order):
 * <ul>
 *   <li>`{Boolean} [ownPayload]` Set to overwrite ownChildPayloads option of the logger.</li>
 *   <li>`{?Object} [payload]` Payload data to add to the message.</li>
 *   <li>`{String} [message]` String to use as primary message.</li>
 *   <li>`{String...|Object} [formatValues]` Additional arguments to format the primary message.</li>
 * </ul>
 * @returns {Logger} Identity.
 * @throws {Error} The logger got closed already and silent has been set to false ({@link Logger#close}).
 * @see Logger#setLevel
 * @see Logger#done
 */
function log(...args) {
  /* eslint no-invalid-this:"off" */
  if (this.loggerScope.closed) {
    let err = new Error("Received log record on closed Logger.");
    this.emit("log-error", {err, args});
    if (this.loggerScope._throw) { throw err; }
    return this;
  }
  let log = new Log(this, this._level, this.loggerScope.logSource ? getCallerPosition(3) : null, args, true);
  this.__lastLog = log;
  this.emit("log", log);
  let transports = this.transports, _len = transports.length;
  for (let i = 0; i < _len; i++) { transports[i].write(log); }
  return this;
}

/*===================================================== Exports  =====================================================*/

export default Logger;
