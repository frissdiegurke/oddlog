import flatstr from "flatstr";

import {clone} from "../utils/object";
import {LIST as LEVELS} from "../constants/std-levels";
import {fastStringEscape} from "../utils/misc";

const SCHEMA_VERSION = 3;

/*===================================================== Classes  =====================================================*/

/**
 * The class for log entries.
 *
 * @class
 */
class Log {

  /**
   * Creates a new log.
   *
   * @param {Logger} logger The logger to whose scope the message shall be bound.
   * @param {Number} level The level of the log.
   * @param {Object} [source] The source location of the caller.
   * @param {Array} args Arguments to be interpreted on-demand by {@link Log#_analyseArguments}.
   * @param {Boolean} useLevel Whether Transports are supposed to filter the log if its level does not match the
   *        transports level restrictions. This is used to force the logging of internal logs.
   * @constructor
   */
  constructor(logger, level, source, args, useLevel) {
    this.logger = logger;
    this.level = level;
    this.date = Date.now();
    this._args = args;
    this._source = source;
    this._useLevel = useLevel;
    this.__data = null; // cached analysis of message method arguments
    this.__msgStringRaw = null; // cached raw record string
    this.__msgStringSimple = null; // cached simple record string
    this.__msg = null; // cached primary message string (formatted)
    this.__pld = null; // cached merged payload
    this.__pldOwn = false; // whether ownership of payload was already gained
    this.__hasMsg = false; // does __msg exist (needed since the value might as well be undefined)
    this.__hasPld = false; // does __pld exist (needed since the value might as well be undefined)
  }

  /**
   * Ensures that the message payload is in logger ownership. Thus it's either been passed with ownPayload set to
   * `true`, or been shallow cloned.
   */
  gainPayloadOwnership() {
    if (this.__pldOwn) { return; }
    this.__pldOwn = true;
    let data = this._analyseArguments();
    if (!data.ownPayload) {
      data.ownPayload = true;
      if (data.payload != null) { data.payload = clone(data.payload); }
    }
  }

  /**
   * Returns the (cached) formatted primary message.
   *
   * @returns {String|undefined} The formatted primary message.
   */
  getFormattedMessage() {
    if (this.__hasMsg) { return this.__msg; }
    this.__hasMsg = true;
    let data = this._analyseArguments();
    if (data.hasOwnProperty("message")) {
      if (data.hasOwnProperty("messageFormatting")) {
        return this.__msg = Reflect.apply(this.logger.messageFormatter, null, data.messageFormatting);
      }
      return this.__msg = data.message;
    }
  }

  /**
   * Returns the (cached) merged payload.
   *
   * @returns {?Object|undefined} The merged payload.
   */
  getPayload() {
    if (this.__hasPld) { return this.__pld; }
    this.__hasPld = true;
    let logger = this.logger, data = this._analyseArguments();
    let payload = logger._getFinalPayload(logger.getPreparedPayload(), data.payload, data.ownPayload);
    data.ownPayload = true;
    return this.__pld = payload;
  }

  /**
   * Returns a (cached) string representation of the message object as following (without whitespaces).
   *
   * <pre>
   * [
   *   {Number}  SCHEMA_VERSION,
   *   {?String} typeKey,
   *   {?Object} meta,
   *   {String}  loggerName,
   *   {Number}  Timestamp,
   *   {Number}  level,
   *   {?Array}  source: [
   *     {String} file,
   *     {Number} row,
   *     {Number} col
   *   ],
   *   {?String} message,
   *   {?Object} [payload]
   * ]
   * </pre>
   *
   * @returns {String} The string representation of the message object.
   */
  getRawRecordString() {
    if (this.__msgStringRaw != null) { return this.__msgStringRaw; }
    let logger = this.logger, src = this._source, msg = this.getFormattedMessage(), payload = this.getPayload();
    let string = logger._prefix + logger.loggerScope._prefix + this.date + "," + this.level + ",";
    string += src == null ? "null," : "[\"" + fastStringEscape(src.file) + "\"," + src.row + "," + src.col + "],";
    string += msg == null ? "null" : "\"" + fastStringEscape(msg) + "\"";
    string += payload !== void 0 ? "," + JSON.stringify(payload) + "]" : "]";
    return this.__msgStringRaw = flatstr(string);
  }

  /**
   * Returns a human readable simple string representation of the message object.
   *
   * @returns {String} The human readable simple string representation of the message object (without payload).
   */
  getSimpleRecordString() {
    if (this.__msgStringSimple != null) { return this.__msgStringSimple; }
    let levelStr = this.level >= LEVELS.length ? LEVELS[LEVELS.length - 1] : LEVELS[Math.ceil(this.level)];
    levelStr = typeof levelStr === "string" ? levelStr.toUpperCase() : "?";
    let string = "[" + new Date(this.date).toISOString() + "] ";
    string += levelStr + " ";
    string += this.logger.loggerScope.name + ": ";
    string += this.getFormattedMessage();
    return this.__msgStringSimple = flatstr(string);
  }

  /**
   * Returns a string representation of the message object according to the provided format.
   *
   * @param {String|Function} format The format to use. Available choices are `"raw"` and `"simple"` or a function that
   *        formats the passed message.
   * @returns {String} The formatted message.
   */
  getRecordString(format) {
    if (typeof format === "function") { return format(this); }
    switch (format) {
      case "simple":
        return this.getSimpleRecordString();
      case "raw":
        return this.getRawRecordString();
      default:
        throw new Error("Record format '" + format + "' not supported.");
    }
  }

  /**
   * Analyses the `args` array to distinguish parameters passed to logging methods of {@link Logger}.
   * The array is processed as following:
   * <ul>
   *   <li>If the current entry is a boolean, interpret as `ownPayload` option.</li>
   *   <li>If the current entry is an object, interpret as `payload`.</li>
   *   <li>If the current entry is a string, interpret as message and all remaining entries as formatting arguments.
   *       Otherwise throw.</li>
   * </ul>
   *
   * @returns {{ownPayload: (Boolean), payload: (?Object|undefined), message: (String|undefined)}} The resulting object.
   * @private
   */
  _analyseArguments() {
    if (this.__data != null) { return this.__data; }
    let args = this._args;
    let result = {ownPayload: this.logger.ownChildPayloads};
    let current = args[0], i = 0;
    if (typeof current === "boolean") {
      result.ownPayload = current;
      current = args[++i];
    }
    if (typeof current === "object") {
      result.payload = current;
      current = args[++i];
    }
    let _len = args.length;
    if (i < _len) {
      if (typeof current !== "string") { throw new Error("Expected message to be a String; Got " + typeof current); }
      result.message = current;
      if (i < _len - 1) {
        result.messageFormatting = args.slice(i); // contains message AND formatting parameters
      }
    }
    return this.__data = result;
  }

}

/*===================================================== Exports  =====================================================*/

export default Log;
export {SCHEMA_VERSION};
