/*===================================================== Classes  =====================================================*/

class RingCacheScope {

  constructor(limit, cleanUp, freeUp) {
    this.limit = limit;
    this.cleanUp = cleanUp;
    this.freeUp = freeUp;
    this.lostRecords = 0;
    this.logs = new Array(limit + 1);
    this.logsStart = 0;
    this.logsEnd = 0;
  }

  push(log) {
    this.logs[this.logsEnd++] = log;
    if (this.logsEnd === this.logs.length) { this.logsEnd = 0; }
    if (this.logsEnd === this.logsStart) {
      this.logsStart++;
      if (this.logsStart === this.logs.length) { this.logsStart = 0; }
      this.lostRecords++;
    }
  }

  child() { return new RingCacheScope(this.limit, this.cleanUp, this.freeUp); }

  flood(log, writeFn) {
    const logs = this.logs, end = this.logsEnd, start = this.logsStart;
    if (start > end) {
      for (let i = start; i < logs.length; i++) { writeFn(logs[i]); }
      for (let i = 0; i < end; i++) { writeFn(logs[i]); }
    } else if (start < end) {
      for (let i = start; i < end; i++) { writeFn(logs[i]); }
    }
    writeFn(log);
    if (this.cleanUp) {
      this.lostRecords = 0;
      this.logsEnd = start;
      if (this.freeUp) { this.logs = new Array(this.logs.length); }
    } else {
      this.push(log);
    }
    const countFlooded = (end > start ? end - start : this.logs.length - start + end) + 1;
    return {
      traced: this.lostRecords + countFlooded,
      backed: countFlooded,
      lost: this.lostRecords,
      limit: this.limit,
    };
  }

}

class RayCacheScope {

  constructor(limit, cleanUp, malloc, windowSize, silentWindow) {
    this.limit = limit;
    this.cleanUp = cleanUp;
    this.malloc = malloc;
    this.windowSize = windowSize;
    this.capSize = limit + windowSize;
    this.silentWindow = silentWindow;
    this.lostRecords = 0;
    this.logs = typeof malloc === "number" ? new Array(malloc) : [];
    this.logsSize = 0;
  }

  push(log) {
    this.logs.push(log);
    if (++this.logsSize === this.capSize) {
      const drop = this.logs.splice(0, this.windowSize);
      this.logsSize -= drop.length;
      this.lostRecords += drop.length;
    }
  }

  child() { return new RayCacheScope(this.limit, this.cleanUp, this.malloc, this.windowSize, this.silentWindow); }

  flood(log, writeFn) {
    const logs = this.logs, _len = this.logsSize;
    for (let i = this.silentWindow && _len > this.limit ? _len - this.limit : 0; i < _len; i++) {
      writeFn(logs[i]);
    }
    writeFn(log);
    if (this.cleanUp) {
      this.lostRecords = 0;
      this.logs = typeof this.malloc === "number" ? new Array(this.malloc) : [];
    } else {
      this.push(log);
    }
    return {
      traced: this.lostRecords + this.logsSize + 1,
      backed: this.logsSize + 1,
      lost: this.lostRecords,
      limit: this.limit,
      window: this.windowSize,
    };
  }

}

/*===================================================== Exports  =====================================================*/

export {RingCacheScope, RayCacheScope};
