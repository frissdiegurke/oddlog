export {assign, assignAll, assignWeak, clone, cloneDeep};

/*==================================================== Functions  ====================================================*/

/**
 * Merge all keys of sources into the target.
 *
 * @param {Object} target The merge target.
 * @param {Object} sources The source objects to merge into the target.
 * @returns {Object} The target.
 */
function assignAll(target, ...sources) {
  if (typeof target !== "object") { return target; }
  let i, _len = sources.length, source;
  for (i = 0; i < _len; i++) {
    source = sources[i];
    for (let key in source) { if (source.hasOwnProperty(key)) { target[key] = source[key]; } }
  }
  return target;
}

function assign(target, source) {
  if (typeof target !== "object") { return target; }
  for (let key in source) { if (source.hasOwnProperty(key)) { target[key] = source[key]; } }
  return target;
}

function assignWeak(target, source) {
  if (typeof target !== "object") { return target; }
  for (let key in source) {
    if (source.hasOwnProperty(key) && !target.hasOwnProperty(key)) { target[key] = source[key]; }
  }
  return target;
}

/**
 * Creates a shallow copy of the passed data. If it's neither an object nor an array, it is returned itself.
 *
 * @param {?Object|Array|*} obj The data to clone.
 * @returns {?Object|Array|*} The cloned data.
 */
function clone(obj) {
  if (obj == null || typeof obj !== "object") { return obj; }
  if (Array.isArray(obj)) {
    if (obj.length > 16) { return obj.slice(); } // see benchmarks
    let arr = [], _len = obj.length, i;
    for (i = 0; i < _len; i++) { arr.push(obj[i]); }
    return arr;
  } else {
    let clone = {};
    for (let key in obj) { if (obj.hasOwnProperty(key)) { clone[key] = obj[key]; } }
    return clone;
  }
}


/**
 * Creates a deep copy of the passed data. Properties that are neither an object nor an array, are assigned themselves.
 *
 * @param {?Object|Array|*} obj The data to clone.
 * @returns {?Object|Array|*} The cloned data.
 */
function cloneDeep(obj) {
  if (obj == null || typeof obj !== "object") { return obj; }
  if (Array.isArray(obj)) {
    let arr = [], _len = obj.length, i;
    for (i = 0; i < _len; i++) { arr.push(cloneDeep(obj[i])); }
    return arr;
  } else {
    let clone = {}, key;
    for (key in obj) { if (obj.hasOwnProperty(key)) { clone[key] = cloneDeep(obj[key]); } }
    return clone;
  }
}
