export {closeAll, eachScopeOnce};

/*==================================================== Functions  ====================================================*/

/**
 * Closes all given loggers and their children. Calls the given callback when all remaining logs are flushed to their
 * respective destination.
 *
 * A closed logger will no longer pass logs to its transports.
 *
 * @param {Boolean} [failSilent=true] Whether to silently ignore future incoming logs. If `false`, an Error will be
 *                  thrown by called logging methods.
 * @param {Logger[]} loggers The loggers to close.
 * @param {Function} cb The callback.
 * @see Logger#close
 */
function closeAll(failSilent, loggers, cb) {
  if (typeof failSilent !== "boolean") { [failSilent, loggers, cb] = [void 0, failSilent, loggers]; }
  let remaining = loggers.length, _len = loggers.length;
  for (let i = 0; i < _len; i++) { loggers[i].close(failSilent, next); }

  function next() { if (!--remaining) { cb(); } }
}

/**
 * Calls the given callback once all given loggers LoggerScopes have emitted the given event once. This function is best
 * called with root loggers and/or child loggers with dedicated LoggerScopes only.
 *
 * @param {Logger[]} loggers The loggers to listen for given event.
 * @param {String} event The name of the event to listen for.
 * @param {Function} cb The callback.
 * @see LoggerScope
 */
function eachScopeOnce(loggers, event, cb) {
  let remaining = loggers.length, _len = loggers.length;
  for (let i = 0; i < _len; i++) { loggers[i].loggerScope.once(event, next); }

  function next() { if (!--remaining) { cb(); } }
}
