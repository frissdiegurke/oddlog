export {getCallerPosition, fastStringEscape};

/*==================================================== Functions  ====================================================*/

function getCallerPosition(deepness) {
  let str = new Error().stack.split("\n", deepness + 1)[deepness];
  if (str == null) { return null; }
  str = str.substring(str.lastIndexOf("(") + 1, str.lastIndexOf(")"));
  let split = str.split(":");
  if (split.length !== 3) { return null; }
  return {
    file: split[0],
    row: +split[1],
    col: +split[2]
  };
}

function fastStringEscape(str) {
  let result = "";
  let someEscape = false;
  let lastIdx = 0;
  let _len = str.length;
  for (let i = 0; i < _len; i++) {
    const code = str.charCodeAt(i);
    if (code === 34 || code === 92) {
      result += str.slice(lastIdx, i) + "\\";
      lastIdx = i;
      someEscape = true;
    } else if (code < 32) {
      result += str.slice(lastIdx, i);
      lastIdx = i + 1;
    }
  }
  return someEscape ? result + str.slice(lastIdx) : str;
}
