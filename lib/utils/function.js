export {identity, self, once, stubTrue, stubFalse};

/*==================================================== Functions  ====================================================*/

function identity(value) { return value; }

// eslint-disable-next-line no-invalid-this
function self() { return this; }

function once(cb) {
  let called = false;
  return function (...args) {
    if (!called) {
      called = true;
      Reflect.apply(cb, null, args);
    }
  };
}

function stubTrue() { return true; }

function stubFalse() { return false; }
