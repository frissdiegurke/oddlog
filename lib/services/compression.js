import fs from "fs";
import zlib from "zlib";

/*===================================================== Exports  =====================================================*/

export {compress, compressAll};

/*==================================================== Functions  ====================================================*/

function compress(from, to, unlink, options, cb) {
  let failed = false;
  let readFile = fs.createReadStream(from);
  let writeFile = fs.createWriteStream(to);
  let gzip = zlib.createGzip(options);

  readFile.once("error", onError);
  writeFile.once("error", onError);
  gzip.once("error", onError);

  writeFile.once("finish", () => { if (!failed) { if (unlink) { fs.unlink(from, cb); } else { cb(); } } });
  readFile.pipe(gzip).pipe(writeFile);

  function onError(err) {
    if (!failed && err != null) {
      failed = true;
      fs.unlink(to, () => cb(err));
    }
  }
}

function compressAll(operations, unlink, options, cb) {
  let remaining = operations.length, operation;
  if (!remaining) { return cb(); }
  for (let i = 0; i < operations.length; i++) {
    operation = operations[i];
    compress(operation.from, operation.to, unlink, options, next);
  }

  function next(err) {
    if (err != null) { return cb(err); }
    if (!--remaining) { cb(); }
  }
}
