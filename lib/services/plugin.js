const PLUGINS = new WeakSet();

/*===================================================== Exports  =====================================================*/

export {mixin};

/*==================================================== Functions  ====================================================*/

/**
 * Initializes the provided plugins.
 *
 * @param {oddlog} oddlog The oddlog instance for which the plugin(s) should be initialized.
 * @param {Array|Function|Object} plugins The plugin(s) to initialize.
 */
function mixin(oddlog, plugins) {
  if (!Array.isArray(plugins)) { plugins = [plugins]; }
  let _len = plugins.length, plugin;
  for (let i = 0; i < _len; i++) {
    plugin = plugins[i];
    if (plugin == null) { continue; }
    if (!PLUGINS.has(plugin)) {
      if (typeof plugin.init === "function") {
        plugin.init(oddlog);
      } else if (typeof plugin === "function") {
        plugin(oddlog);
      } else {
        throw new Error("Plugin " + plugin + " is not compatible with this version of OddLog");
      }
      PLUGINS.add(plugin);
    }
  }
}
