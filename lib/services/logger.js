/* eslint-disable complexity */
import {hostname, release} from "os";
import {format} from "util";

import {DEFAULT_VALUE as DEFAULT_LEVEL} from "../constants/std-levels";
import {getLevelValue} from "./levels";
import {clone} from "../utils/object";
import {identity} from "../utils/function";
import * as STANDARD_TRANSFORMER from "../constants/std-transformer";

import Logger from "../classes/Logger";
import LoggerScope from "../classes/LoggerScope";
import {SCHEMA_VERSION} from "../classes/Log";
import {fastStringEscape} from "../utils/misc";

const DEFAULT_META =
  "[\"" + fastStringEscape(release()) + "\",\"" + fastStringEscape(hostname()) + "\"," + process.pid + "]";

/*===================================================== Exports  =====================================================*/

export {createLogger};

/*==================================================== Functions  ====================================================*/

/**
 * This function provides an interface to easily create Logger instances without the need to specify every option and
 * handle Transport creation.
 *
 * @param {String} name The name for the new logger.
 * @param {?Object} [options] The options to define the logger:
 * <ul>
 *   <li>`{(?Object)[]} [transports=[{type:"stream",level:TRACE}]]` A list of options to derive transports of.</li>
 *   <li>`{!Object} [transformer={fn,err,req,res}]` Transformer mapping to apply to message payload.</li>
 *   <li>`{Boolean} [ownPayload=false]` Whether the `payload` may be modified by the Logger.</li>
 *   <li>`{Boolean} [ownChildPayloads=false]` Whether payloads of children and messages may be modified by the
 *       Logger. This can be overwritten respectively within child and logging methods.</li>
 *   <li>`{Boolean} [singleTransform=true]` Whether transformations shall only be executed at message level. If set to
 *       false, logger level payload transformations will be cached. Caching can speed up the logging (especially for
 *       computational complex transformers), but will freeze the value of logger payload for all its messages.</li>
 *   <li>`{?Object} [meta=[platform,host,pid]]` Meta information to add to messages. The default values are
 *       os.release(), os.hostname() and process.pid. Set to `null` to disable meta information.</li>
 *   <li>`{?String} [typeKey="_type"]` The key to use for a payloads (nested) type identifications. Supported types by
 *       oddlog can be specified within any payload data at this key. Such types are "error", "value" and "plain". If
 *       set to null, only some keys (such as "err") will identify a default type; Everything else is handled like the
 *       type "plain".</li>
 *   <li>`{?Boolean} [logSource]` Whether to add the source code location of the caller to messages. This might slow
 *       down your application and is not recommended for production. If not set, it defaults to `false` iff no
 *       environment variable (DEBUG/TRACE) is matching.</li>
 *   <li>`{?Function} [messageFormatter=util.format]` The formatting function to use for primary message texts.</li>
 *   <li>`{Number|String} [level=INFO]` The level to log with using the {@link Logger#log} method.</li>
 *   <li>`{Boolean} [shy=false]` Whether to set some sane library default options for transports (fallback-level: WARN
 *       and format: simple). Usage of DEBUG/TRACE environment variables that match this logger nullifies the effect.
 *       This option is intended to be used by libraries (at least in production).</li>
 * </ul>
 * @param {?Object} [payload] Payload to add to messages of the logger and its children.
 * @returns {Logger} The new Logger instance.
 * @see createTransport
 */
function createLogger(name, options, payload) {
  if (options == null) { options = {}; }
  const meta = options.hasOwnProperty("meta") ? options.meta : DEFAULT_META;
  const transformer = options.hasOwnProperty("transformer") ? options.transformer : STANDARD_TRANSFORMER;
  const level = getLevelValue(options.level, DEFAULT_LEVEL);
  const ownChildPayloads = !!options.ownChildPayloads;
  const singleTransform = options.hasOwnProperty("singleTransform") ? !!options._singleTransform : true;
  const logSource = options.logSource;
  const shy = !!options.shy;
  let messageFormatter;
  if (options.hasOwnProperty("messageFormatter")) {
    messageFormatter = typeof options.messageFormatter === "function" ? options.messageFormatter : identity;
  } else {
    messageFormatter = format;
  }
  if (!options.ownPayload && payload != null) { payload = clone(payload); }
  let prefix = "[" + SCHEMA_VERSION + ",";
  prefix += options.hasOwnProperty("typeKey") ? fastStringEscape(options.typeKey) + "," : "\"_type\",";
  prefix += meta == null ? "null," : (typeof meta === "string" ? meta : JSON.stringify(meta)) + ",\"";
  const loggerScope = new LoggerScope(name, logSource);
  let logger = new Logger(
    prefix, payload, void 0, transformer, level, shy, ownChildPayloads, singleTransform, logSource, messageFormatter,
    loggerScope, true
  );
  if (options.hasOwnProperty("transports")) {
    if (options.transports != null) { logger.addTransports(options.transports); }
  } else {
    logger.addTransport({type: "stream", level: exports.TRACE});
  }

  loggerScope.on("drain", () => logger.emit("drain"));
  loggerScope.once("end", () => logger.emit("end"));
  loggerScope.once("close", () => logger.emit("close"));

  return logger;
}
