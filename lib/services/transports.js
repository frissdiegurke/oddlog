import {create as createFileTransport} from "../classes/transports/FileTransport";
import {create as createStreamTransport} from "../classes/transports/StreamTransport";
import {create as createCapTransport} from "../classes/transports/CapTransport";
import {create as createMemoryTransport} from "../classes/transports/MemoryTransport";
import {create as createSplitTransport} from "../classes/transports/SplitTransport";
import {create as createTriggerTransport} from "../classes/transports/TriggerTransport";
import {create as createFileRotateTransport} from "../classes/transports/FileRotateTransport";
import {create as getVoidTransport} from "../classes/transports/VoidTransport";

/**
 * Maps each registered transport type to its instantiation function.
 *
 * @type {Object}
 */
const MAPPED = {
  stream: createStreamTransport,
  file: createFileTransport,
  cap: createCapTransport,
  memory: createMemoryTransport,
  split: createSplitTransport,
  trigger: createTriggerTransport,
  "file-rotate": createFileRotateTransport,
  void: getVoidTransport,
  instance: instanceAlias,
};

/*===================================================== Exports  =====================================================*/

export {MAPPED, createTransport, defineType, getFallbackType};

/*==================================================== Functions  ====================================================*/

/**
 * Transport creation alias if the transport was already instantiated.
 *
 * @param {{instance:Transport}} options The options that contains the `instance` field.
 * @returns {Transport} The instance that was provided.
 * @private
 */
function instanceAlias(options) { return options.instance; }

/**
 * Define a new type of Transport.
 *
 * @param {String} key The identifier of the type.
 * @param {Function} createFn The function that creates instances of the type.
 * @param {Boolean} [overwrite=false] Whether to overwrite, if a type with the same key already exists.
 */
function defineType(key, createFn, overwrite) {
  if (MAPPED.hasOwnProperty(key) && !overwrite) { throw new Error("Type '" + key + "' already registered."); }
  MAPPED[key] = createFn;
}

/**
 * Derives the transport type from transport definition options.
 *
 * @param {Object} options The options to derive the transport type of.
 * @returns {String} The transport type to use for the passed options.
 * @private
 */
function getFallbackType(options) {
  if (options.hasOwnProperty("stream")) { return "stream"; }
  if (options.hasOwnProperty("path")) {
    if (options.hasOwnProperty("size") || options.hasOwnProperty("interval")) { return "file-rotate"; }
    return "file";
  }
  if (options.hasOwnProperty("cap")) { return "cap"; }
  if (options.hasOwnProperty("split")) { return "split"; }
  if (options.hasOwnProperty("threshold") || options.hasOwnProperty("replicate")) { return "trigger"; }
  if (options.hasOwnProperty("store")) { return "memory"; }
  if (options.hasOwnProperty("instance")) { return "instance"; }
  throw new Error("Transport type could not be derived by attributes.");
}

/**
 * Creates a new Transport instance according to given options.
 * If the options contain a identifying key (e.g. `stream`, `path`, `trigger`), the type field can be omitted.
 * Otherwise the type key must be an identifier of a Transport class.
 * The options are passed to the constructor of the Transport class for type-specific impacts.
 *
 * @param {Object} options The options to derive the Transport of.
 * @returns {Object} The Transport instance.
 * @see defineType
 */
function createTransport(options) {
  if (options == null) { return null; }
  let type = options.type || getFallbackType(options);
  let transport = MAPPED[type];
  if (typeof transport !== "function") { throw new TypeError("Unknown transport type: " + type); }
  return transport(options, createTransport);
}
