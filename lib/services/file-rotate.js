import fs from "fs";
import path from "path";

import {compressAll} from "./compression";
import {renameSeriesReverse, unlinkAll} from "./file-system";

const SUFFIX_REGEX = /^\.([1-9]|\d{2,})(\.gz)?$/;

/*===================================================== Exports  =====================================================*/

export {rotate};

/*==================================================== Functions  ====================================================*/

function rotate(dir, baseFile, tmpFileName, maxFiles, compress, cb) {
  getOperations(dir, baseFile, tmpFileName, maxFiles, (err, operations) => {
    if (err != null) { return cb(err); }
    let {removes, renamesReverse, compressions} = operations;
    unlinkAll(removes, (err) => {
      if (err != null) { return cb(err); }
      renameSeriesReverse(renamesReverse, (err) => {
        if (err != null) { return cb(err); }
        if (compress) {
          compressAll(compressions, true, typeof compress === "object" ? compress : null, next);
        } else {
          next();
        }

        function next(err) {
          if (err != null) { return cb(err); }
          cb(null, renamesReverse[0] && renamesReverse[0].to);
        }
      });
    });
  });
}

function getOperations(dir, baseFile, tmpFileName, maxFiles, cb) {
  fs.readdir(dir, (err, files) => {
    if (err != null) { return cb(err); }
    let renames = [], removes = [], compressions = [], _len = files.length;
    for (let i = 0; i < _len; i++) {
      let file = files[i];
      if (file === tmpFileName) {
        renames[0] = {from: path.join(dir, file), to: path.join(dir, baseFile)};
      } else if (file === baseFile) {
        if (maxFiles <= 1) {
          removes.push(path.join(dir, file));
        } else {
          let to = path.join(dir, baseFile + ".1");
          renames[1] = {from: path.join(dir, file), to};
          compressions.push({from: to, to: to + ".gz"});
        }
      } else if (file.startsWith(baseFile)) {
        let match = SUFFIX_REGEX.exec(file.substring(baseFile.length));
        if (match != null) {
          let num = +match[1] + 1, suffix = match[2] || "";
          if (maxFiles <= num) {
            removes.push(path.join(dir, file));
          } else {
            renames[num] = {from: path.join(dir, file), to: path.join(dir, baseFile + "." + num + suffix)};
          }
        }
      }
    }
    cb(null, {removes, renamesReverse: renames, compressions});
  });
}
