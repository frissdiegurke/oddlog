import fs from "fs";

/*===================================================== Exports  =====================================================*/

export {unlinkAll, renameSeriesReverse};

/*==================================================== Functions  ====================================================*/

function unlinkAll(files, cb) {
  let remaining = files.length;
  if (!remaining) { return cb(); }
  for (let i = 0; i < files.length; i++) { fs.unlink(files[i], next); }

  function next(err) {
    if (err != null) { return cb(err); }
    if (!--remaining) { cb(); }
  }
}

function renameSeriesReverse(operations, cb) {
  let renameIndex = operations.length - 1;
  rename();

  function rename(err) {
    if (err != null) { return cb(err); }
    if (renameIndex < 0) { return cb(); }
    let operation = operations[renameIndex--];
    if (operation != null) { fs.rename(operation.from, operation.to, rename); }
  }
}
