import {MAPPED} from "../constants/std-levels";

/*===================================================== Exports  =====================================================*/

export {nameToValue, getLevelValue};

/*==================================================== Functions  ====================================================*/

function nameToValue(name) { return MAPPED.hasOwnProperty(name) ? MAPPED[name] : -1; }

function getLevelValue(nameOrValue, fallback) {
  if (typeof nameOrValue === "string" && MAPPED.hasOwnProperty(nameOrValue)) { return MAPPED[nameOrValue]; }
  return typeof nameOrValue === "number" ? nameOrValue : fallback;
}
