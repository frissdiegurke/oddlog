const TB = 1000000000000, GB = 1000000000, MB = 1000000, KB = 1000;
const SIZE_FACTOR = { // factor to derive number of bytes
  b: 1 / 8,
  B: 1, "": 1,
  K: KB, KB, kB: KB,
  M: MB, MB,
  G: GB, GB,
  T: TB, TB,
  KiB: 1024,
  MiB: 1048576,
  GiB: 1073741824,
  TiB: 1099511627776
};

const MINUTE = 60000, WEEK = 604800000, MONTH = 2592000000, YEAR = 31536000000;
const TIME_FACTOR = { // factor to derive number of milliseconds
  ns: 1 / KB,
  ms: 1, "": 1,
  s: KB,
  m: MINUTE, min: MINUTE,
  h: 3600000,
  d: 86400000, // 24h
  w: WEEK, week: WEEK, weeks: WEEK, // 7d
  month: MONTH, months: MONTH, // 30d
  a: YEAR, year: YEAR, years: YEAR // 365d
};

/*===================================================== Exports  =====================================================*/

export {SIZE_FACTOR, TIME_FACTOR};
