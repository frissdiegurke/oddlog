export const LIST = ["silent", "trace", "debug", "verbose", "info", "warn", "error", "fatal"];
export const MAPPED = {
  silent: 0,
  trace: 1,
  debug: 2,
  verbose: 3,
  info: 4,
  warn: 5,
  error: 6,
  fatal: 7,
};
export const DEFAULT_NAME = "info";
export const DEFAULT_VALUE = MAPPED[DEFAULT_NAME];
