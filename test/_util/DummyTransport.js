import MemoryTransport from "../../lib/classes/transports/MemoryTransport";

export default class DummyTransport extends MemoryTransport {

  constructor(options) { super(options || {}, []); }

  attach(logger) { MemoryTransport.prototype.attach.call(this, logger); }

}
