import {createLogger} from "../../lib";
import {LIST} from "../../lib/constants/std-levels";

describe("log", () => {
  describe("level", () => {
    let logger, store;

    beforeEach(() => {
      store = [];
      logger = createLogger("some-id", {transports: [{store, level: 0}]});
    });
    afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

    it("should be passed to log instance according to method called", () => {
      LIST.length.should.be.greaterThan(0);
      for (let level of LIST) {
        logger[level]("test " + level);
        store[store.length - 1].level.should.be.equal(LIST.indexOf(level));
      }
    });
  });
});
