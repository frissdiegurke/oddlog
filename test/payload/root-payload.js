import _ from "lodash";

import {createLogger} from "../../lib/index";
import DummyWriteStream from "../_util/DummyWriteStream";

const FAKE_STREAMS = [{stream: new DummyWriteStream(false)}];

describe("root payload", () => {
  describe("null payload", () => {
    it("should not throw", () => {
      createLogger("some-id", {transports: FAKE_STREAMS}, null).info({some: "data"});
      createLogger("some-id", {transports: FAKE_STREAMS, ownPayload: true}, null)
        .child({hello: "world"}).info({answer: 42});
    });
  });

  describe("with own-payload disabled", () => {
    let loggerPayload;
    let logger;

    beforeEach(() => {
      loggerPayload = {some: "data", answer: 42};
      logger = createLogger("some-id", {transports: FAKE_STREAMS}, loggerPayload);
    });

    it("should not get modified", () => {
      let PL = _.clone(loggerPayload);
      logger.info({answer: 21, lorem: "ipsum"});
      logger.child({answer: 21, lorem: "ipsum"});
      loggerPayload.should.deepEqual(PL);
    });

    it("should get cloned", () => {
      logger.info({answer: 21, lorem: "ipsum"});
      logger.child({answer: 21, lorem: "ipsum"});
      loggerPayload.should
        .not.equal(logger.__mergedPayload)
        .deepEqual(logger.__mergedPayload);
    });

    it("should get cloned if transformed", () => {
      logger = createLogger("some-id", {transports: FAKE_STREAMS, singleTransform: false}, loggerPayload);
      logger.info({answer: 21, lorem: "ipsum"});
      logger.child({answer: 21, lorem: "ipsum"});
      loggerPayload.should
        .not.equal(logger.__mergedPayload)
        .deepEqual(loggerPayload);
    });
  });

  describe("with own-payload enabled", () => {
    let loggerPayload;
    let logger;

    beforeEach(() => {
      loggerPayload = {some: "data", answer: 42};
      logger = createLogger(
        "some-id", {transports: [{stream: new DummyWriteStream()}], ownPayload: true}, loggerPayload
      );
    });

    it("should not get modified", () => {
      let PL = _.clone(loggerPayload);
      logger.info({answer: 21, lorem: "ipsum"});
      logger.child({answer: 21, lorem: "ipsum"});
      loggerPayload.should.deepEqual(PL);
    });

    it("should not get cloned", () => {
      logger.info({answer: 21, lorem: "ipsum"});
      logger.child({answer: 21, lorem: "ipsum"});
      loggerPayload.should.equal(logger.__mergedPayload);
    });
  });
});
