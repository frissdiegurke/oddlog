import os from "os";

import {createLogger} from "../../lib";
import {updateEnv} from "../../lib/services/env";
import DummyWriteStream from "../_util/DummyWriteStream";

describe("logger", () => {
  describe("shy", () => {
    let logger = null, store;

    afterEach((done) => logger == null || logger.loggerScope.closed ? done() : logger.close(done));

    it("should not log below WARN", () => {
      store = [];
      logger = createLogger("some-id", {shy: true, transports: [{store}]});
      logger.info("test");
      store.length.should.be.equal(0);
    });

    it("should log at and above WARN", () => {
      store = [];
      logger = createLogger("some-id", {shy: true, transports: [{store}]});
      logger.warn("test");
      store.length.should.be.equal(1);
      logger.error("test");
      store.length.should.be.equal(2);
    });

    it("should make StreamTransport default to simple format", () => {
      const stream = new DummyWriteStream();
      logger = createLogger("some-id", {shy: true, transports: [{stream}]});
      logger.warn("Hello %s!", "X");
      stream.received.should.be
        .equal("[" + new Date(logger.__lastLog.date).toISOString() + "] WARN some-id: Hello X!" + os.EOL);
    });

    it("should ignore shy when env variable is matching", () => {
      const stream = new DummyWriteStream();
      process.env.DEBUG = "some-id";
      updateEnv();
      logger = createLogger("some-id", {shy: true, transports: [{stream}]});
      logger.warn("Hello %s!", "X");
      stream.received.should.endWith(",\"Hello X!\"]" + os.EOL, "StreamTransport does not use raw format");
      logger.debug("test debug");
      stream.received.should.endWith(",\"test debug\"]" + os.EOL, "Transport neglects debug level messages");
      logger.trace("test trace");
      stream.received.should.not.endWith(",\"test trace\"]" + os.EOL, "Transport does process trace messages");
      Reflect.deleteProperty(process.env, "DEBUG");
      updateEnv();
    });
  });
});
