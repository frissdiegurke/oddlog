import {createLogger, ERROR, INFO, SplitTransport} from "../../lib/index";
import DummyTransport from "../_util/DummyTransport";

describe("transports", () => {

  describe("SplitTransport", () => {
    let transport0, transport1, transport2;

    beforeEach(() => {
      transport0 = new DummyTransport();
      transport1 = new DummyTransport();
      transport2 = new DummyTransport();
    });

    it("should add logs to the correct store", (done) => {
      const logger = createLogger(
        "some-id",
        {
          transports: [{
            split: [INFO, ERROR],
            transports: [{instance: transport0}, {instance: transport1}, {instance: transport2}],
          }],
        }
      );
      logger.transports[0].should.be.instanceOf(SplitTransport);
      logger.debug("debug");
      logger.verbose("verbose");
      transport0.store.length.should.be.equal(2);
      transport1.store.length.should.be.equal(0);
      transport2.store.length.should.be.equal(0);
      logger.info("info");
      logger.warn("warn");
      transport0.store.length.should.be.equal(2);
      transport1.store.length.should.be.equal(2);
      transport2.store.length.should.be.equal(0);
      logger.error("error");
      logger.fatal("fatal");
      transport0.store.length.should.be.equal(2);
      transport1.store.length.should.be.equal(2);
      transport2.store.length.should.be.equal(2);
      logger.close(done);
    });

    it("should work with null transport definitions", (done) => {
      const logger = createLogger(
        "some-id",
        {
          transports: [{
            split: [INFO, ERROR],
            transports: [{instance: transport0}, null, {instance: transport2}],
          }],
        }
      );
      logger.transports[0].should.be.instanceOf(SplitTransport);
      logger.debug("debug");
      logger.verbose("verbose");
      transport0.store.length.should.be.equal(2);
      transport2.store.length.should.be.equal(0);
      logger.info("info");
      logger.warn("warn");
      transport0.store.length.should.be.equal(2);
      transport2.store.length.should.be.equal(0);
      logger.error("error");
      logger.fatal("fatal");
      transport0.store.length.should.be.equal(2);
      transport2.store.length.should.be.equal(2);
      logger.close(done);
    });

  });

});
