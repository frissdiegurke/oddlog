import {createLogger, INFO, TRACE} from "../../lib/index";

describe("transports", () => {

  describe("NestedTransport (CapTransport)", () => {
    let store0, store1, logger;

    beforeEach(() => {
      store0 = [];
      store1 = [];
      logger = createLogger("some-id", {
        transports: [{level: TRACE, cap: INFO, transports: [{store: store0}, {store: store1}]}]
      }, {hello: "world"});
    });
    afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

    it("should forward messages to all transports", (done) => {
      store0.length.should.be.equal(0);
      store1.length.should.be.equal(0);
      logger.info("test");
      logger.close(() => {
        store0.length.should.be.equal(1);
        store1.length.should.be.equal(1);
        done();
      });
    });
  });

});
