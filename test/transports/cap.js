import {createLogger, INFO, TRACE} from "../../lib/index";

describe("transports", () => {

  describe("CapTransport", () => {
    let store, logger;

    beforeEach(() => {
      store = [];
      logger = createLogger("some-id", {
        transports: [{level: TRACE, cap: INFO, transports: [{store}]}]
      }, {hello: "world"});
    });
    afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

    it("should forward logs below and at cap-level", () => {
      store.length.should.be.equal(0);
      logger.info("test at");
      store.length.should.be.equal(1);
      logger.verbose("test below");
      store.length.should.be.equal(2);
    });

    it("should not forward logs above cap-level", () => {
      store.length.should.be.equal(0);
      logger.warn("test above");
      store.length.should.be.equal(0);
    });

  });

});
