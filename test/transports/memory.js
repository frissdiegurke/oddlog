import {createLogger, TRACE} from "../../lib/index";

describe("transports", () => {

  describe("MemoryTransport", () => {
    let store, logger;

    beforeEach(() => {
      store = [];
      logger = createLogger("some-id", {transports: [{level: TRACE, store}]}, {hello: "world"});
    });
    afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

    it("should add logs to store", (done) => {
      store.length.should.be.equal(0);
      logger.info("test 0");
      store.length.should.be.equal(1);
      logger.verbose("test 1");
      store.length.should.be.equal(2);
      done();
    });

  });

});
