import fs from "fs";
import os from "os";
import path from "path";
import zlib from "zlib";
import uuid from "uuid/v4";

import {createLogger} from "../../lib/index";

const TMP_DIR = path.resolve(__dirname, "..", ".tmp");

describe("transports", () => {

  before((done) => {
    fs.access(TMP_DIR, fs.R_OK, (err) => {
      if (err == null) { return done(); }
      fs.mkdir(TMP_DIR, done);
    });
  });
  after((done) => {
    fs.access(TMP_DIR, fs.R_OK, (err) => err == null ? fs.rmdir(TMP_DIR, done) : done());
  });

  describe("FileRotateTransport", () => {

    describe("stream", () => {
      let streamClosed, file, logger;

      beforeEach(() => {
        streamClosed = false;
        file = path.join(TMP_DIR, uuid());
        logger = createLogger("some-id", {transports: [{path: file, size: "1M"}]}, {hello: "world"});
        const active = logger.transports[0]._scope.active;
        active.prependOnceListener("ready", () => {
          active.stream.prependOnceListener("finish", () => streamClosed = true);
        });
      });
      afterEach((done) => fs.exists(file, (bool) => bool ? fs.unlink(file, done) : done()));
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should end on close", (done) => {
        logger.info("test");
        logger.close(() => {
          streamClosed.should.be.true();
          done();
        });
      });

      it("should not end without explicit close", (done) => {
        logger.info("test");
        process.nextTick(() => {
          streamClosed.should.be.false();
          done();
        });
      });

      it("of named child should end child on close of parent", (done) => {
        let child = logger.child("sub", {test: true});
        child.info("test");
        logger.close(() => {
          streamClosed.should.be.true();
          done();
        });
      });

      it("of parent should not end on close of named child", (done) => {
        let child = logger.child("sub", {test: true});
        child.info("test");
        child.close(() => {
          streamClosed.should.be.false();
          done();
        });
      });
    });

    describe("rotation", () => {
      let streamClosed, file, logger;

      beforeEach(() => {
        streamClosed = false;
        file = path.join(TMP_DIR, uuid());
        logger = createLogger("some-id", {transports: [{path: file, size: 1}]}, {hello: "world"});
        const active = logger.transports[0]._scope.active;
        active.prependOnceListener("ready", () => {
          active.stream.prependOnceListener("finish", () => streamClosed = true);
        });
      });
      afterEach((done) => {
        next(null, 0);

        function next(err, i) {
          if (err != null) { return done(err); }
          const f = i === 0 ? file : file + "." + i;
          fs.exists(f, (bool) => {
            if (!bool) { return done(); }
            fs.unlink(f, (err) => next(err, i + 1));
          });
        }
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should end the previous stream", (done) => {
        logger.once("transport:file-rotate:rotation:end", () => {
          streamClosed.should.be.true();
          logger.close(() => fs.unlink(file + ".1", done));
        });
        logger.info("test");
        logger.info("test");
      });
    });

    describe("compression", () => {
      let file, logger;

      beforeEach(() => {
        file = path.join(TMP_DIR, uuid());
        logger = createLogger("some-id", {transports: [{path: file, size: 1, compress: true}]}, {hello: "world"});
      });
      afterEach((done) => {
        next(null, 0);

        function next(err, i) {
          if (err != null) { return done(err); }
          const f = i === 0 ? file : file + "." + i + ".gz";
          fs.exists(f, (bool) => {
            if (!bool) { return done(); }
            fs.unlink(f, (err) => next(err, i + 1));
          });
        }
      });
      afterEach((done) => logger.loggerScope.closed ? done() : logger.close(done));

      it("should work with compression", (done) => {
        logger.once("transport:file-rotate:rotation:end", () => {
          fs.readFile(file + ".1.gz", (err, content) => {
            if (err != null) { throw err; }
            content.toString().should.not.startWith("[");
            zlib.gunzip(content, (err, data) => {
              if (err != null) { throw err; }
              data.should.startWith("[");
              data.should.endWith("]" + os.EOL);
              done();
            });
          });
        });
        logger.info("test");
        logger.info("test2");
      });
    });

  });

});
